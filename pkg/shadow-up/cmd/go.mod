module git.nemunai.re/lectures/adlin/pkg/shadow-up

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	golang.org/x/sys v0.0.0-20220224120231-95c6836cb0e7 // indirect
	gopkg.in/fsnotify.v1 v1.4.7
)
