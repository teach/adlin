module git.nemunai.re/srs/adlin/pkg/minichecker/cmd

go 1.14

require (
	github.com/go-ping/ping v1.1.0
	github.com/miekg/dns v1.1.58
	gopkg.in/fsnotify.v1 v1.4.7
)

require github.com/fsnotify/fsnotify v1.5.1 // indirect
