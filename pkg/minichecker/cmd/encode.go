package main

import (
	"bytes"
	"crypto/ed25519"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

var (
	Login   string
	KeySign string
	target  string
)

func getCollectorPublicKey() (key []byte, err error) {
	var resp *http.Response
	resp, err = http.Get(target + "/api/collector_info")
	if err != nil {
		return
	}

	defer resp.Body.Close()

	rd := base64.NewDecoder(base64.StdEncoding, json.NewDecoder(resp.Body).Buffered())
	return ioutil.ReadAll(rd)
}

type SendMeta struct {
	Time  time.Time `json:"time"`
	Login string    `json:"login"`
	Test  string    `json:"test,omitempty"`
}

type SendContent struct {
	Meta    []byte `json:"meta"`
	Data    []byte `json:"data"`
	Sign    []byte `json:"sign"`
	Key     []byte `json:"key"`
	KeySign string `json:"keysign"`
}

func encodeData(meta SendMeta, v interface{}) (data SendContent, err error) {
	if meta.Time.IsZero() {
		meta.Time = time.Now()
	}
	var b []byte
	b, err = json.Marshal(meta)
	if err != nil {
		return
	}
	data.Meta = []byte(base64.StdEncoding.EncodeToString(b))

	b, err = json.Marshal(v)
	if err != nil {
		return
	}
	data.Data = []byte(base64.StdEncoding.EncodeToString(b))

	data.Sign = ed25519.Sign(myprivkey, append(data.Meta, data.Data...))
	data.Key = myprivkey.Public().(ed25519.PublicKey)
	data.KeySign = KeySign

	return
}

func sendData(data SendContent) (err error) {
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(data)

	var resp *http.Response
	resp, err = http.Post(target+"/remote", "application/json", b)
	if err != nil {
		return
	}

	resp.Body.Close()

	return
}
