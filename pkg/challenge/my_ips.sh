#!/bin/sh

# /proc/cmdline parser (from Gentoo Wiki)
cmdline() {
    local value
    value=" $(cat /proc/cmdline) "
    value="${value##* $1=}"
    value="${value%% *}"
    [ "$value" != "" ] && echo "$value"
}

IPS=$(curl -s -f "http://172.23.0.1/api/students/$(cmdline adlin.login)/ips")

if [ $? -ne 0 ]
then
    echo -e "Utilisez l'IP \e[01m$(cmdline adlin.ip)\e[0m pour vous connecter au réseau."
else
    echo "Voici la liste des IP qui vous ont été attribuées :"
    for KEY in vlan0 wg0 vlan7
    do
        [ $(echo "${IPS}" | jq -r ".${KEY}") != "null" ] && {
            echo -en " - \e[01m"
            case $KEY in
                vlan0) echo -n "Services Router";;
                wg0) echo -n "VPN";;
                vlan7) echo -n "Internet Router";;
                *) echo -n "Autre IP";;
            esac
            echo -en " :\e[0m "
            echo "${IPS}" | jq -r ".${KEY}"
        }
    done

    [ $# -gt 0 ] && [ "$1" == "-a" ] && {
        INFOS=$(curl -s -f "http://172.23.0.1/api/students/$(cmdline adlin.login)/")

        echo
        echo -e "La MAC bénéficiant d'une protection est \e[01m$(echo "${IPS}" | jq -r .mac)\e[0m"
    }
fi

echo
echo -e "\e[41;33;01m /!\\ \e[0m Attention à bien préciser la plage indiquée dans la topologie !"
echo
