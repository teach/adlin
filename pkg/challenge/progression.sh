#!/bin/sh

# /proc/cmdline parser (from Gentoo Wiki)
cmdline() {
    local value
    value=" $(cat /proc/cmdline) "
    value="${value##* $1=}"
    value="${value%% *}"
    [ "$value" != "" ] && echo "$value"
}

PROGRESSION=$(curl -s -f "http://172.23.0.1/api/students/$(cmdline adlin.login)/progress")

[ $? -ne 0 ] && {
    echo "Vous ne semblez pas connecté.e. Passez au moins la première étape pour afficher votre progression."
    exit
}

for CHID in 0 1 2 3 4 5 10 11 12 6 7 8 9
do
    [ $(echo "${PROGRESSION}" | jq -r ".\"${CHID}\".time") != "null" ] && echo -ne " \e[42;30;01m " || echo -ne " \e[41;01m "
    case $CHID in
        0) echo -n "rooted";;
        6) echo -n "Hidden bonus";;
        7) echo -n "ICMP bonus";;
        8) echo -n "Disk bonus";;
        9) echo -n "Email bonus";;
        10) echo -n "WG tunnel up";;
        11) echo -n "SSH key shared";;
        12) echo -n "The end";;
        *) echo -n "Step $CHID";;
    esac
    echo -en " \e[0m  "
done

echo
