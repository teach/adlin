package main

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha512"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path"
	"text/template"
)

const pxeUserTplPath = "pxelinux.cfg/tpl"
const ipxeUserTplPath = "pxelinux.cfg/tpl.ipxe"
const pxeUserPath = "pxelinux.cfg"

func RegisterUserMAC(ent ARPEntry, ip net.IP, username, remoteAddr string) error {
	if err := registerUser(ipxeUserTplPath, remoteAddr, fmt.Sprintf("%02x:%02x:%02x:%02x:%02x:%02x.ipxe", ent.HWAddress[0], ent.HWAddress[1], ent.HWAddress[2], ent.HWAddress[3], ent.HWAddress[4], ent.HWAddress[5]), username, ip); err != nil {
		return err
	} else {
		return registerUser(pxeUserTplPath, remoteAddr, fmt.Sprintf("%02x-%02x-%02x-%02x-%02x-%02x-%02x", ent.HWType, ent.HWAddress[0], ent.HWAddress[1], ent.HWAddress[2], ent.HWAddress[3], ent.HWAddress[4], ent.HWAddress[5]), username, ip)
	}
}

func initrd_name(remote string) string {
	log.Println("initrd_name:", remote)
	initrd := hmac.New(md5.New, []byte(loginSalt))
	initrd.Write([]byte(remote))
	return fmt.Sprintf("%x", initrd.Sum(nil))
}

func registerUser(tplPath, remoteAddr string, filename string, username string, ip net.IP) error {
	if pxeTplCnt, err := ioutil.ReadFile(path.Join(tftpDir, tplPath)); err != nil {
		return err
	} else if userfd, err := os.OpenFile(path.Join(tftpDir, pxeUserPath, filename), os.O_RDWR|os.O_CREATE, 0644); err != nil {
		return err
	} else {
		defer userfd.Close()

		initrd := initrd_name(remoteAddr)

		err := os.MkdirAll(path.Join(tftpDir, "shadows", initrd), 0755)
		if err != nil {
			return err
		}

		fdlogin, err := os.Create(path.Join(tftpDir, "shadows", initrd, "whois"))
		if err != nil {
			return err
		}
		defer fdlogin.Close()
		fdlogin.Write([]byte(username + "\n" + remoteAddr + "\n" + ip.String()))

		fd, err := os.Create(path.Join(tftpDir, "shadows", initrd, "challenge-initrd.img"))
		if err != nil {
			return err
		}
		defer fd.Close()

		err = copy_challenge(fd)
		if err != nil {
			return err
		}

		pkey := hmac.New(sha512.New512_224, []byte(loginSalt))
		pkey.Write([]byte(username))

		if len(ip.To4()) != 4 {
			return fmt.Errorf("Unable to assign a protected IP.")
		}

		if pxeTmpl, err := template.New("pxeUser").Parse(string(pxeTplCnt)); err != nil {
			return err
		} else if err := pxeTmpl.Execute(userfd, map[string]string{
			"username": username,
			"initrd":   initrd,
			"pkey":     fmt.Sprintf("%x", pkey.Sum(nil)),
			"ip":       ip.String(),
		}); err != nil {
			return err
		}
	}
	return nil
}
