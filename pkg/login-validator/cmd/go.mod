module git.nemunai.re/srs/adlin/pkg/login-validator

go 1.17

require (
	github.com/cavaliergopher/cpio v1.0.1
	github.com/go-ldap/ldap/v3 v3.4.6
	github.com/jcmturner/gokrb5/v8 v8.4.4
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20221128193559-754e69321358 // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.5 // indirect
	github.com/google/uuid v1.3.1 // indirect
	github.com/hashicorp/go-uuid v1.0.3 // indirect
	github.com/jcmturner/aescts/v2 v2.0.0 // indirect
	github.com/jcmturner/dnsutils/v2 v2.0.0 // indirect
	github.com/jcmturner/gofork v1.7.6 // indirect
	github.com/jcmturner/rpc/v2 v2.0.3 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/net v0.10.0 // indirect
)
