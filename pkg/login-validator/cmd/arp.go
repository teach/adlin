package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"strings"
)

var ARPTable string = "/proc/net/arp"

type ARPEntry struct {
	IP        net.IP
	HWType    int
	Flags     int
	HWAddress net.HardwareAddr
	Mask      string
	Device    string
}

func ARPAnalyze() (ents []ARPEntry, err error) {
	var content []byte
	if content, err = ioutil.ReadFile(ARPTable); err != nil {
		return
	}

	for _, line := range strings.Split(string(content), "\n") {
		f := strings.Fields(line)
		if len(f) > 5 {
			var e ARPEntry

			if _, err := fmt.Sscanf(f[1], "0x%x", &e.HWType); err != nil {
				continue
			}
			if _, err := fmt.Sscanf(f[2], "0x%x", &e.Flags); err != nil {
				continue
			}

			e.IP = net.ParseIP(f[0])
			if e.HWAddress, err = net.ParseMAC(f[3]); err != nil {
				continue
			}
			e.Mask = f[4]
			e.Device = f[5]

			ents = append(ents, e)
		}
	}

	return
}

func ARPContainsIP(ents []ARPEntry, ip net.IP) *ARPEntry {
	for i, e := range ents {
		if e.IP.Equal(ip) && (e.Flags == 2 || e.Flags == 6) {
			return &ents[i]
		}
	}

	return nil
}
