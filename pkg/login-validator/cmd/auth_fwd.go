package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"
)

type FWDAuth struct {
	URI *url.URL
}

type loginForm struct {
	Username string
	Password string
}

func (f FWDAuth) checkAuth(username, password string) (res bool, err error) {
	lf := loginForm{username, password}
	j, err := json.Marshal(lf)
	if err != nil {
		return false, err
	} else if r, err := http.NewRequest("POST", f.URI.String(), bytes.NewReader(j)); err != nil {
		return false, err
	} else if resp, err := http.DefaultClient.Do(r); err != nil {
		return false, err
	} else {
		resp.Body.Close()

		return resp.StatusCode < 400, err
	}
}
