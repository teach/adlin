package main

type AuthMethod interface {
	checkAuth(username, password string) (bool, error)
}

type NoAuth struct{}

func (NoAuth) checkAuth(username, password string) (res bool, err error) {
	return true, nil
}
