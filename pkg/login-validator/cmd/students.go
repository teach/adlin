package main

import (
	"bufio"
	"encoding/csv"
	"os"
	"path/filepath"
)

type Student struct {
	Lastname  string
	Firstname string
	Login     string
	EMail     string
	Phone     string
}

func readStudentsList(studentsFile string) (stds []Student, err error) {
	if studentsFile, err = filepath.Abs(studentsFile); err != nil {
		return
	} else if fi, err := os.Open(studentsFile); err != nil {
		return nil, err
	} else {
		r := csv.NewReader(bufio.NewReader(fi))
		if list, err := r.ReadAll(); err != nil {
			return nil, err
		} else {
			for _, i := range list {
				stds = append(stds, Student{
					Lastname:  i[0],
					Firstname: i[1],
					Login:     i[2],
					EMail:     i[3],
					Phone:     i[4],
				})
			}

			return stds, nil
		}
	}
}
