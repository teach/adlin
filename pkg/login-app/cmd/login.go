package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

type loginForm struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Force    *bool  `json:"force,omitempty"`
}

func checkLogin(username, password string, force *bool) (int, error) {
	j, err := json.Marshal(loginForm{Username: username, Password: password, Force: force})
	if err != nil {
		return 0, err
	}

	resp, err := http.Post(URLLogin, "application/json", bytes.NewReader(j))
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	cnt, _ := ioutil.ReadAll(resp.Body)
	return resp.StatusCode, errors.New(string(cnt))
}
