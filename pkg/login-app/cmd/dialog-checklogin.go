package main

import (
	"math"
	"time"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

func CreateCheckDialog(app *tview.Application) *tview.Box {
	progress := 0
	box := tview.NewBox().
		SetBorder(true).
		SetTitle(" SRS Adlin - Login ").
		SetDrawFunc(func(screen tcell.Screen, x int, y int, width int, height int) (int, int, int, int) {
			tview.Print(screen, "Please wait", x, y+2, width-2, tview.AlignCenter, tcell.ColorYellow)
			tview.Print(screen, "Connecting to login server...", x, y+3, width-2, tview.AlignCenter, tcell.ColorYellow)

			// Draw the progress bar
			for cx := x + 2; cx < x+width-2; cx++ {
				if (cx-x)*100/(width-2) > progress {
					screen.SetContent(cx, y+5, tview.BoxDrawingsLightHorizontal, nil, tcell.StyleDefault.Background(tcell.ColorBlack))
				} else {
					screen.SetContent(cx, y+5, ' ', nil, tcell.StyleDefault.Background(tcell.ColorBlue))
				}
			}

			return x, y, width, height
		})

	app.SetRoot(modal(box, 40, 8), true)
	app.SetFocus(box)

	go func() {
		for i := 0; i < 422; i += 1 {
			progress = int(math.Floor(math.Log(float64(i)*8)*16 - 30))
			time.Sleep(64 * time.Millisecond)
			app.Draw()
		}
	}()

	return box
}
