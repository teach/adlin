package main

import (
	"fmt"

	"github.com/rivo/tview"
)

func CreateErrMsgDialog(app *tview.Application, err error) {
	textView := tview.NewTextView().
		SetDynamicColors(true).
		SetRegions(true).
		SetChangedFunc(func() {
			app.Draw()
		})

	form := tview.NewForm().
		AddButton("Authenticate me", func() {
			askLogin(app)
		})

	flex := tview.NewFlex().
		AddItem(nil, 0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(textView, 12, 1, false).
			AddItem(form, 1, 1, false), 37, 1, false).
		AddItem(nil, 0, 1, false)

	flex.SetBorder(true).
		SetTitle(" SRS Adlin - Login ")

	fmt.Fprintf(textView, "\nAn error occurs:\n\n[red]%s\n\n[yellow]Press Enter to retry", err.Error())

	app.SetRoot(modal(flex, 42, 15), true)
	app.SetFocus(form)
}
