module git.nemunai.re/lectures/adlin/pkg/login-app/cmd

go 1.16

require (
	github.com/gdamore/tcell/v2 v2.7.1
	github.com/rivo/tview v0.0.0-20240225120200-5605142ca62e
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
)
