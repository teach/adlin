package main

import (
	"github.com/rivo/tview"
)

func CreateForceLoginDialog(app *tview.Application, username, password string, next func(string, string, *bool)) {
	modal := tview.NewModal().
		SetText("You are already registered on a different machine.\n\nIf you continue, the other machine will no longer be able to use its dedicated IPs due to network safeties in place.\n\nIf the other machine doesn't work or you are Ok to lost your progression, just force this host as your new main host.\nIf you want a secondary machine to play without erasing your progression on the main one, continue without enforcing network safety.").
		AddButtons([]string{"Cancel", "Force this host as main", "Boot without network safety"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			var force bool
			if buttonLabel == "Force this host as main" {
				force = true
				next(username, password, &force)
			} else if buttonLabel == "Boot without network safety" {
				force = false
				next(username, password, &force)
			} else {
				askLogin(app)
			}
		})

	app.SetRoot(modal, true)
	app.SetFocus(modal)
}
