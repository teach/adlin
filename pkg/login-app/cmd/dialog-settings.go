package main

import (
	"os/exec"

	"github.com/rivo/tview"
)

var LastKeymap = 0

func goToSettings(app *tview.Application) {
	var form *tview.Form
	form = tview.NewForm().
		AddDropDown("Keymap", []string{"QWERTY us", "AZERTY fr", "BÉPO", "Colemak us"}, LastKeymap, nil).
		AddButton("    Save config    ", func() {
			if kbmap, _ := form.GetFormItemByLabel("Keymap").(*tview.DropDown).GetCurrentOption(); LastKeymap != kbmap {
				file := "/usr/share/keymaps/xkb/"

				switch kbmap {
				case 1:
					file += "fr.map.gz"
				case 2:
					file += "fr-bepo.map.gz"
				case 3:
					file += "us-colemak.map.gz"
				default:
					file += "us.map.gz"
				}

				exec.Command("/usr/bin/loadkeys", file).Run()
				LastKeymap = kbmap
			}
			askLogin(app)
		})
	form.GetFormItemByLabel("Keymap").(*tview.DropDown).SetFieldWidth(25)
	form.SetBorder(true).SetTitle(" SRS Adlin - Settings ")

	app.SetRoot(modal(form, 25, 7), true)
	app.SetFocus(form)

	return
}
