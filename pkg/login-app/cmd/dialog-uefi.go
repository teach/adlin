package main

import (
	"github.com/rivo/tview"
)

func CreateUEFIDialog(app *tview.Application, next func()) {
	modal := tview.NewModal().
		SetText("This machine does not boot in UEFI mode!\nYou should reboot now, enter UEFI Setup (F2) and disable Legacy boot. Or choose another machine, that boot in UEFI.").
		AddButtons([]string{"Reboot", "Ignore"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			if buttonLabel == "Reboot" {
				app.Stop()
			} else if buttonLabel == "Ignore" {
				next()
			}
		})

	app.SetRoot(modal, true)
	app.SetFocus(modal)
}
