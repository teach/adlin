package main

import (
	"github.com/rivo/tview"
)

func CreateLoginDialog(app *tview.Application, next func(username, password string)) {
	var form *tview.Form
	form = tview.NewForm().
		AddInputField("Login", "", 27, nil, nil).
		AddPasswordField("Password", "", 27, '*', nil).
		AddButton("Authenticate me", func() {
			next(
				form.GetFormItemByLabel("Login").(*tview.InputField).GetText(),
				form.GetFormItemByLabel("Password").(*tview.InputField).GetText(),
			)
		}).
		AddButton("Settings...", func() {
			goToSettings(app)
		})
	form.SetBorder(true).SetTitle(" SRS Adlin - Login ")

	app.SetRoot(modal(form, 40, 9), true)
	app.SetFocus(form)

	return
}
