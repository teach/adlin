package main

import (
	"flag"
	"log"
	"net"
	"net/netip"
)

func main() {
	var ifacestr = flag.String("iface", "eth0", "Interface to use")
	var ipspoof = flag.String("ip-spoof", "", "IP to ARP spoof")
	flag.Parse()

	iface, err := net.InterfaceByName(*ifacestr)
	if err != nil {
		log.Fatal("Unable to find interface to do ARP spoof:", err)
	}

	ipS, err := netip.ParseAddr(*ipspoof)
	if err != nil {
		log.Fatalf("No IP given to ARP spoof (%s). Skipping it", err.Error())
	}

	// Start ARP spoofing
	ARPSpoof(iface, ipS)
}
