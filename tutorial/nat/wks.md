\newpage

Stations de travail
===================

Plusieurs stations de travail sont simulées au sein de votre machine
virtuelle. Celles-ci vont régulièrement faire des requêtes DHCP afin d'obtenir
une IP.

Pour vous simplifier les opérations de débug, vous pouvez ajouter une carte
ethernet à la machine virtuelle afin d'avoir accès en SSH à l'un des
postes. L'IP sera alors indiquée sur l'écran de la VM, au démarrage : il s'agit
de l'interface `eth1`.

Vous pouvez également ajouter une troisième carte réseau pour la brancher à
d'autres machines virtuelle de hyperviseur, en passant par un pont. Toutes vos
VM reliées à ce pont bénéficieront alors du réseau des postes de travail de
votre *entreprise*.

DHCP
----

N'ayant pas, pour le moment, d'IPv6 sur votre réseau ; vous devez mettre en
place un serveur DHCP sur votre réseau afin que vos collaborateurs puissent
utiliser facilement le réseau.

Une fois configuré, vous pouvez tenter de vous connecter aux services web (vous
devriez avoir `wget` sur les stations de travail accessibles).


Zone démilitarisée et filtrage
------------------------------

Il est risqué d'exposer des ports d'administration sur Internet ou de permettre
à des zones sensibles d'être accédées par rebond. Définissez quelques règles de
filtrage sur le routeur afin notamment :

* d'empêcher les connexions au serveur de base de données, seules les connexions
  réalisées depuis le réseau des serveurs resteront disponibles ;
* de n'exposer le port SSH que depuis le réseau interne (pas depuis l'interface
  connectée à Internet).
