\newpage

Démarrer sur le SI du jour
==========================

Présentation du système d'information
-------------------------------------

Le système d'information que vous allez avoir à gérer aujourd'hui est de taille
moyenne. Vous aurez en votre possession :

  - un serveur DNS résolveur,
  - un serveur DNS faisant autorité,
  - un serveur de base de données,
  - plusieurs serveurs web (servant respectivement votre vitrine,
[Miniflux](https://miniflux.app/), ainsi que [Matrix](https://matrix.org/)),
  - de nombreux postes de travail.

Vous êtes l'administrateur réseau de l'entreprise et l'on vous demande de
connecter tous ces services **correctement**.

Voici un schéma de l'infrastructure complète dont vous allez disposer en
démarrant l'ISO.

![Architecture réseau à produire](topologie.png "Topologie")

L'image ISO que vous avez récupérée met à votre disposition tout ce système
d'information, déjà en partie configuré pour sa partie logicielle, il ne reste
plus qu'à éditer la configuration du routeur.

::::: {.warning}

Contrairement au précédent TP, la majorité des modifications que vous allez
effectuer ne seront pas persistantes d'un reboot à l'autre. À chaque
redémarrage de la machine virtuelle, vous retomberez dans un état initial.
Seuls quelques éléments comme le token de votre tunnel, la configuration du
routeur et le contenu de la base de données persistent.

:::::

Accéder à la machine virtuelle
------------------------------

Une fois la machine virtuelle démarrée, vous devriez voir apparaître l'IP qu'a
obtenu la machine sur votre réseau :

```
    You didn't define your token to connect the network. Please run here `join-maatma` and then reboot.
    4: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
        link/ether 42:2c:63:f1:5a:49 brd ff:ff:ff:ff:ff:ff
	    inet 10.42.23.211/24 brd 10.42.23.255 scope global eth0
            valid_lft forever preferred_lft forever
        inet6 fe80::4534:6558:22cf:f2cd/64 scope link
            valid_lft forever preferred_lft forever
    Attachez une seconde carte ethernet à la VM pour pouvoir vous connecter à un poste de travail.
```

Cette IP est l'IP attribuée à votre routeur par votre hyperviseur, obtenue par
DHCP.

::::: {.warning}

Une ligne de commande est disponible au sein de la machine virtuelle à des fins
de débogage, si nécessaire. Vous ne devriez normalement pas avoir à interagir
avec celle-ci.

:::::

### Tunnel Maatma

Pour retrouver votre tunnel sur Internet, vous devez, dans la console de la
machine virtuelle, utiliser la commande `join-maatma`.

Vous pouvez aussi écrire directement dans le fichier persistant :

<div lang="en-US">
```
    echo M0nT0k3n > /var/lib/adlin/wireguard/adlin.token
```
</div>

Une fois le token renseigné, vous devez redémarrer la machine afin qu'il soit
pris en compte.


### Connexions SSH

Vous pouvez vous connecter en utilisant le compte `root` et le mot de passe
`adlin2024`. Comme au précédent TP, si vous disposez d'une ou plusieurs [clefs
SSH enregistrées au CRI](https://cri.epita.fr/users/nemunaire/ssh-keys/),
celles-ci sont automatiquement ajoutées aux différents serveurs.

Notez que vous n'avez pas accès à la machine hébergeant la base de données, ni
à celle hébergeant le lecteur de flux RSS.


Objectif du TP
--------------

Toutes les IP notées sur le schéma ont déjà été configurées par votre
administrateur système en suivant vos instructions. Votre tâche est de rendre
accessible les services web sur internet ainsi qu'aux stations de
travail. Tout le monde doit avoir accès à internet et utiliser le serveur de
noms local (les serveurs ayant déjà été configurés ainsi, il faudra simplement
s'assurer que ce soit également le cas des stations de travail).

Étant donné le caractère éphémère de vos actions, la réalisation d'un
*Playbook* Ansible semble plutôt adaptée !


### Mots de passes

Tous les mots de passes initiaux sont `adlin2024`, pour tous les services.

3 bases de données Postgres sont à votre disposition pour vos différents
services :

  - `matrix`
  - `miniflux`
  - `website`

Pour chacune, un utilisateur du même nom existe pour s'y connecter à distance.


Au secours ça ne marche pas !
-----------------------------

Pas de panique !

Quelques commandes sont à votre disposition dans le shell de la machine
virtuelle afin de vérifier que tout fonctionne correctement.


### Monitoring

Utilisez la commande `diagnostic` afin d'avoir une vue d'ensemble des
éventuels problèmes.

Tout est vert ? Toutes les machines sont opérationnelles !

Une ou plusieurs machines sont rouges ? il y a effectivement un problème avec
les machines concernées. Tentez de redémarrer une fois.

Si le problème persiste, vous pouvez formater votre disque en utilisant la
commande `raz-my-dd`. Ce qui vous permettra de retrouver la configuration
d'origine.

::::: {.more}

Pour les plus téméraires, les journaux des machines sont regroupés dans
`/var/log` accessible uniquement dans la console de la machine virtuelle.

:::::

### Retrouver mes IP

Vous ne pouvez pas simplement taper `ip a` dans le terminal de votre machine
virtuelle. Utilisez plutôt la commande `welcome` qui affichera à nouveau le
message d'accueil avec les différentes IP associées à vos machines.


### Rejoindre Maatma

Utilisez la commande `join-maatma` dans la console de la machine virtuelle,
puis redémarrez la machine.

Vous pouvez aussi passer par l'interface de votre routeur pour ajouter une
interface 'Wireguard VPN'. Utilisez les informations contenues sur la page
[Tunnels de Maatma](https://adlin.nemunai.re/maatma/tunnels) pour remplir les
différents champs.


### `join-maatma` retourne une erreur 400

Vérifiez votre token, il n'est pas valide.
