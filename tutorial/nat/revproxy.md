\newpage

Reverse-proxy
=============

Plusieurs services sur un port ?
--------------------------------

Sur Internet, rares sont les IP qui n'hébergent qu'un seul service. Très
souvent, une armée de serveurs est placée derrière, comme ici, et permet de
distribuer plusieurs services sur la même IP, sans que les utilisateurs n'aient
à changer leur port de connexion.

La distinction est généralement réalisée sur le nom de domaine
accédé. Cependant, les protocoles IP, TCP ou UDP ne transportent pas cette
information. C'est donc dans la couche applicative que le nom de domaine est
passé.

Par exemple, vous pouvez observer ce comportement facilement avec `curl` :

<div lang="en-US">
```
    42sh$ dig +short adlin.nemunai.re
	82.64.31.248
    42sh$ curl https://adlin.nemunai.re
	    <head><title>Index of /</title></head>

    42sh$ curl -k https://82.64.31.248
	    <img style="height: calc(100vh - 5px); max-width: 100vw;" src="//ouaset.masr.nemunai.re/public/vache.svg" alt="Bienvenue sur rhakotis">

    42sh$ curl -k -H "Host: adlin.nemunai.re" https://82.64.31.248
	    <head><title>Index of /</title></head>
```
</div>

En HTTP[^https], on face un en-tête `Host`, contenant le nom de domaine. Le
serveur web (généralement ici utilisé comme proxy inverse) oriente la
distribution de son contenu en fonction de cet en-tête.

[^https]: En HTTPS, le certificat est distribué avant que le client n'ait pu
    envoyer de contenu. Le nom est alors passé dans un champ d'extension de
    TLS. Voir le [RFC 6066](https://tools.ietf.org/html/rfc6066) à propos de
    SNI.

À vous maintenant d'installer un reverse-proxy (`nginx` par exemple), pour vous
permettre d'accéder aux différents services, sur le port 80.

En utilisant le résolveur local, vous pouvez vérifier le fonctionnement de
votre reverse-proxy avec (soit sur une station de travail, soit sur le routeur
directement) :

<div lang="en-US">
```sh
    42sh$ curl http://news.adlin.p0m.fr/
        <title>Connexion - Miniflux</title>

    42sh$ curl http://matrix.adlin.p0m.fr/
        <title>Matrix</title>

    42sh$ curl http://www.adlin.p0m.fr/
        <title>Ma Vitrine</title>
```
</div>

Notez que ce n'est pas le rôle du routeur/pare-feu de faire
reverse-proxy, vous devriez utiliser le serveur web de votre vitrine
pour gérer les autres domaines.


Connexions sécurisées
---------------------

Maintenant que vos services écoutent en HTTP, il est temps de leur
ajouter un certificat, afin que toutes les connexions soient
sécurisées.

Plusieurs solutions s'offrent à vous dans ce genre de situation :

- laisser le reverse-proxy établir les connexions TLS (en envoyant
  notamment le certificat), puis transmettre les paquets reçus et
  déchiffrés, sur le réseau interne en clair. On parle alors de
  Terminaison TLS.

- utiliser un proxy SNI pour acheminer vers la bonne machine interne,
  le trafic entrant, en le gardant chiffré jusqu'au
  bout. L'orientation se fait grâce à l'extension TLS SNI.

On préférera la première solution pour avoir un meilleur contrôle des
machines directement exposées, mais il faut bien avoir conscience que
cela peut créer un goulot d'étranglement.

La deuxième solution est plus adaptée lorsque l'on souhaite depuis le
réseau interne, directement aux machines, sans passer par un
reverse-proxy, ou lorsque le réseau externe est en IPv6.
Dans ce cas, pour se passer de la terminaison TLS, chaque serveur doit
avoir son propre certificat à disposition.
