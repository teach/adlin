\newpage

Rendu
=====

## Modalité de rendu

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <adlin@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.


## Tarball

Tous les fichiers identifiés comme étant à rendre pour ce TP sont à
placer dans une tarball (pas d'archive ZIP, RAR, ...).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires,
en fonction de votre organisation ou de vos choix) :

<div lang="en-US">
```
login_x-TP3/hosts
login_x-TP3/site.yml
login_x-TP3/basis.yml
login_x-TP3/router.yml
login_x-TP3/matrix.yml
login_x-TP3/name-server.yml
login_x-TP3/vitrine.yml
login_x-TP3/group_vars/all
login_x-TP3/roles/common/tasks/main.yml
login_x-TP3/roles/matrix-synapse/defaults/main.yml
login_x-TP3/roles/matrix-synapse/tasks/main.yml
login_x-TP3/roles/matrix-synapse/templates/homeserver.yaml.j2
login_x-TP3/roles/name-server/defaults/main.yml
login_x-TP3/roles/name-server/tasks/main.yml
login_x-TP3/roles/name-server/templates/myzone.j2
login_x-TP3/roles/revproxy/defaults/main.yml
login_x-TP3/roles/revproxy/tasks/main.yml
login_x-TP3/roles/revproxy/templates/nginx.conf.j2
...
```
</div>


### Check-list minimale

- Votre routeur route les paquets IPv4 et IPv6,
- votre routeur fournit du DHCP sur le réseau des stations de travail, afin qu'elles puissent accéder à Internet normalement et aux serveurs,
- votre routeur filtre les paquets entrants (IPv4, IPv6) selon la politique que vous avez défini,
- votre routeur bloque les connexions entrantes vers le réseau des stations de travail,
- votre routeur effectue du NAT en IPv4 pour les serveurs et les stations de travail.
\
- Votre vitrine est exposée en HTTP et HTTPS,
- les options HTTPS ont été choisies avec soin, selon les recommandations de l'ANSSI,
- le visiteur est redirigé systématiquement vers la version HTTPS,
- le visiteur est redirigé vers `www.login_x.srs.p0m.fr` lorsqu'il visite `login_x.srs.p0m.fr`,
- `news.login_x.srs.p0m.fr` affiche miniflux,
- `matrix.login_x.srs.p0m.fr` est prêt.
\
- Votre serveur de nom de domaines est accessible en TCP et UDP,
- votre nom de domaine se résout depuis un résolveur public,
\
- La configuration de tous les serveurs accessibles respectent les recommandations de l'ANSSI,
- votre IPv6 publique peut évoluer en changeant simplement une variable `group_vars/all`.
\
- Vous avez utilisé des
  [rôles](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html)
  pour rendre vos recettes Ansible réutilisables.


## Signature du rendu

Deux méthodes sont utilisables pour signer votre rendu :

* signature du courriel ;
* signature de la tarball.

Dans les deux cas, si vous n'en avez pas déjà une, vous devrez créer une clef
PGP à **votre nom et prénom**.

Pour valider la signature, il est nécessaire d'avoir reçu la clef publique
**séparément**. Vous avez le choix de la téléverser sur un serveur de clefs,
soit de me fournir votre clef en main propre, soit de l'envoyer dans un
courriel **distinct**.

### Signature du courriel

Une version récente de [Thunderbird](https://www.thunderbird.net/fr/) vous
permettra d'envoyer des courriels signés. Si vous n'avez qu'une version
ancienne, l'extension [Enigmail](https://enigmail.net) est très bien réputée
pour signer ses mails depuis Thunderbird. Bien entendu, de nombreuses autres
solutions sont disponibles.

Utilisez le service automatique <signcheck@nemunai.re> pour savoir si votre
courriel est correctement signé et que je suis en mesure de vérifier la
signature.
