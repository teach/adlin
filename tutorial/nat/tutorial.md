---
title: Administration Linux avancée -- TP n^o^ 3
subtitle:
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Vendredi 1 avril 2022
abstract: |
  Durant ce troisième TP, nous allons faire un peu plus de réseau !

  \vspace{1em}

  Tous les éléments de ce TP (exercices et projet) sont à rendre à
  <adlin@nemunai.re> au plus tard le dimanche 1 mai 2022 à 23
  h 42. Consultez la dernière section de chaque partie pour plus d'information
  sur les éléments à rendre.

  En tant que personnes sensibilisées à la sécurité des échanges électroniques,
  vous devrez m'envoyer vos rendus signés avec votre clef PGP. Pensez à
  [me](https://keys.openpgp.org/search?q=nemunaire%40nemunai.re)
  faire signer votre clef et n'hésitez pas à [faire signer votre
  clef](https://www.meetup.com/fr/Paris-certification-de-cles-PGP-et-CAcert/).
...
