\newpage

Routage
=======

Votre entreprise dispose d'un modeste routeur utilisant
[OpenWrt](https://openwrt.org/). Il s'agit d'une petite distribution Linux
mettant à disposition une interface web pour configurer son routeur
facilement[^facilement].

[^facilement]: Si vous préférez, ce TP existe aussi en utilisant une simple
    distribution Debian comme routeur. C'est tout aussi efficace et les mêmes
    technologies sont en jeu. Demandez à votre professeur l'ISO si vous êtes
    intéressés !

Au sein du noyau Linux, le [projet `netfilter`](https://netfilter.org/) est
l'implémentation de pare-feu standard. Il s'agit d'un mécanisme de filtrage des
paquets réseau basés sur des règles, chargées par l'espace utilisateur dans le
noyau. On utilise pour cela les programmes `nft` (du projet `nftables`), ou de
manière historique `iptables`.

Les deux projets ont une manière de fonctionner très similaire, mais il est
important de ne pas les utiliser en même temps, sous peine de voir apparaître
des problèmes incompréhensibles à déboguer, plus ou moins aléatoirement.


## Réseau fonctionnel ?

Première étape : avez-vous vérifié l'état du réseau sur le routeur ?

* Peut-il accéder à Internet ?
* Peut-il accéder aux serveurs ?
* Peut-il accéder aux stations de travail ?

Dans la configuration initiale attendue de la machine virtuelle, les réponses
que vous devriez obtenir sont :

* Oui, le routeur a accès à Internet, mais ne peut pas résoudre de noms de
  domaine.
* Oui, les serveurs répondent aux ping et sont joignables.
* Non, les stations de travail n'obtiennent pas d'IP, on ne peut pas les
  contacter.


## Donner accès à Internet

Votre administrateur système vous assure que le serveur de noms est bien lancé
et configuré comme demandé.

Deux éléments de configuration vont devoir être mis en place sur le routeur
pour corriger[^fix] cette situation.

[^fix]: Oui, il s'agit bien ici de non configuration : ne cherchez pas de
    mesquinerie de la part de l'auteur du TP.

Après cette configuration, toutes les machines (serveurs et stations de
travail) pourront accéder à Internet.

Test à passer :

<div lang="en-US">
```
    router$ dig +short @172.23.42.2 adlin.nemunai.re
	82.64.31.248
```
</div>

Vous pouvez utiliser, par exemple `tcpdump`, pour comprendre ce qu'il se passe
sur votre routeur :

<div lang="en-US">
```
    router# tcpdump -i ethsrv
    router# tcpdump -i eth0 udp and port 53
```
</div>

::::: {.warning}

Le serveur DNS résolveur met en cache les problèmes qu'il rencontre. Il vous
faudra sans doute patienter une bonne minute, une fois que vous aurez la bonne
configuration, afin qu'il tente à nouveau de contacter un serveur de noms pour
faire une résolution. C'est un comportement normal.\

Si vous êtes impatient, vous pouvez aussi vous connecter sur la machine
hébergeant le résolveur afin de le redémarrer manuellement :

```sh
router$ ssh root@ns
resolvsrv$ service unbound restart
```

:::::

## Accéder aux autres serveurs du parc

Depuis le routeur, vous pouvez vous SSH en utilisant le nom d'hôte attribué aux
machines :

<div lang="en-US">
  - `ssh root@matrix`
  - `ssh root@ns-auth`
  - `ssh root@web`
</div>


Depuis l'extérieur, vous devez utiliser le routeur comme *bastion*. Avec SSH,
utilisez l'option `-J` pour désigner une machine intermédiaire qui servira à
accéder à la machine cible :

<div lang="en-US">
```
    42sh$ ssh -J root@$EXT_ROUTER root@172.23.42.3
```
</div>


## Exposer les services

### Localement

Vous avez compris comment vos machines peuvent accéder à Internet sans avoir
pour autant d'IP routable sur Internet. Cependant, si cela répond parfaitement
à une utilisation de type station de travail, vos serveurs web doivent être
accessibles sur Internet.

En utilisant une règle de `netfilter`, rendez vos trois serveurs web accessibles
depuis l'interface externe du routeur. Après configuration, depuis un
navigateur sur votre poste, vous devriez pouvoir accéder à :

<div lang="en-US">
* `http://$EXT_ROUTER_IP:8000/` : Vitrine
* `http://$EXT_ROUTER_IP:8080/` : Miniflux
* `http://$EXT_ROUTER_IP:8008/` : Matrix
</div>

::::: {.warning}

Il s'agit là seulement d'un test avec l'IPv4 de votre routeur. Il n'est pas
encore demandé de faire cela sur l'IPv6 routable sur Internet.

Il ne s'agit donc pas d'utiliser l'IP de Maatma, vous devez tester cela sur
votre propre installation, avec l'IPv4 donné par l'interface `eth0`.

:::::

### Utiliser Miniflux

Utilisez le nom d'utilisateur `adeline` pour vous connecter à
miniflux. N'oubliez pas de changer le mot de passe avant que quelqu'un
d'autre s'en charge à votre place !

Si vos serveurs ont bien accès à Internet, vous pourrez mettre à jour
la liste des flux pré-enregistrés dans miniflux, afin de faire un peu
de veille !
