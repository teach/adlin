Dépannage DNS
-------------

### Problèmes de cache

Le DNS, comme on a pu le voir, est une structure hiérarchisée et fortement
sollicitée.

Afin d'éviter un engorgement excessif de certains serveurs (`.com` par
exemple !) et de mieux répartir la charge entre les acteurs, un système de
cache permet de garder en mémoire les derniers enregistrements consultés, dans
la limite de leur durée de vie, annoncé par le champ TTL de chaque
enregistrement.

<div lang="en-US">
```
42sh$ dig adlin.nemunai.re A
[...]
;; ANSWER SECTION:
adlin.nemunai.re.	78942	IN	A	82.64.151.41
[...]
```
</div>

`78942` c'est le nombre de secondes durant lequel l'enregistrement `A` pour le
domaine `adlin.nemunai.re.` sera retourné par le résolveur, sans effectuer tout
le cheminement de résolution.

C'est vous dans le fichier de zone qui pouvez indiquer la durée que vous
souhaitez utiliser : une durée faible vous permettra de plus facilement tester
différents paramètres ou de changer régulièrement les enregistrements (pour
tester c'est plutôt pratique), mais cela peut engendre une sollicitation
excessive de votre serveur. À l'inverse, un cache long permet de se prémunir
contre des problèmes de disponibilité, et tend à réduire les sollicitation du
serveur faisant autorité.

Tous les résolveurs étant indépendant, il n'existe pas de moyen[^PURGE_CACHE] pour vider le
cache d'un résolveur que vous ne contrôlez pas, afin qu'il n'ait plus en cache
une donnée potentiellement erronée. Il faut attendre la fin du TTL sur chaque
résolveur.

[^PURGE_CACHE]: Ce n'est pas exactement vrai, Cloudflare et Google ont des
  formulaires pour forcer le vidage du cache de leurs résolveurs publics :
  <https://1.1.1.1/purge-cache/> et
  <https://developers.google.com/speed/public-dns/cache>.

Ce problème cause parfois de gros problème, ce fût notamment le [cas pour Slack
en
2021](https://slack.engineering/what-happened-during-slacks-dnssec-rollout/).

Si vous êtes confronté à un problème avec votre zone, commencez par regarder le
TTL de l'enregistrement retourné par votre résolveur local.


### Outils

Lorsque vous allez établir votre délégation, vous pourriez utiliser l'outil
[Zonemaster](https://www.zonemaster.fr/fr/) pour diagnostiquer d'éventuels
problèmes avec vos enregistrements, les *GLUE records*, ...

Pour diagnostiquer des problèmes liés à DNSSEC, vous pouvez utiliser :

- [DNSViz](https://dnsviz.net/)
- [DNSSEC Analyzer](https://dnssec-analyzer.verisignlabs.com/)


#### happyDomain

Enfin, pour gérer votre fichier de zone, vous pourriez vouloir passer par une
interface web. Vous pouvez regarder du côté
d'[happyDomain](https://www.happydomain.org/), que vous pouvez installer à côté
de votre serveur DNS faisant autorité (exemple pour
[knot](https://help.happydomain.org/fr/deploy/knot/)).
