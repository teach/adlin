---
title: Administration Linux avancée -- TP n^o^ 2
subtitle: "Maatma : l'hébergeur DIY"
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Jeudi 2 mars 2023
abstract: |
  Durant ce deuxième TP, nous allons préparer un serveur pour réponde à des
  services classiques d'Internet. Nous appréhenderons le fonctionnement des
  noms de domaine et des certificats TLS, au travers de Maatma, votre
  hébergeur.

  \vspace{1em}

  Ce TP est la partie découverte du 1er projet qui sera à rendre pour le
  **mercredi 29 mars 2023 à 23 h 42**. Nous verrons au prochain TP comment
  automatiser toutes les étapes d'aujourd'hui grâce à Ansible. Vous serez notés
  d'une part sur votre avancement dans les étapes de ce TP, et également sur la
  qualité du rendu de l'automatisation de ces mêmes étapes, via Ansible (que
  l'on verra au prochain TP).
...
