\newpage

Démarrer sur le SI du jour
==========================

Accéder à la machine virtuelle
------------------------------

Une fois la machine virtuelle démarrée, vous pouvez vous y connecter en `root`
avec le mot de passe `adlin2024`.

::::: {.more}

Vous pouvez également démarrer en mode *single user*, mais comme votre disque
n'est sans doute pas encore utilisable à ce stade, vous ne pourrez pas changer
le mot de passe `root`. Vous pourrez néanmoins effectuer les étapes de
configuration du disque décrites plus loin, sous ce mode, puis redémarrer.

Lorsque vous avez accès à la ligne de commande du *bootloader*, vous pouvez
généralement ajouter l'option `init=/bin/sh` à la ligne de commande du noyau,
afin de lancer un shell au lieu du système d'init habituel.

:::::


Se mettre à l'aise
------------------

Un serveur SSH est déjà installé et configuré pour vous permettre d'avoir une
interface plus agréable que celle de votre hyperviseur.\

::::: {.warning}

Si votre machine virtuelle est directement connectée au réseau
local (sans passer par un NAT), n'oubliez pas de modifier le mot de passe
`root` pour éviter que n'importe qui sur le réseau local (et ayant accès à ce
TP), ne rentre sur votre machine.

:::::

Afin de vous faciliter la configuration de la machine par la suite, vos clefs
SSH publiques, [déclarées au
CRI](https://cri.epita.fr/users/nemunaire/ssh-keys/), sont automatiquement
ajoutées à l'utilisateur `root`. Rendez-vous dans l'interface du CRI pour les
mettre à jour si besoin. Notez qu'elles ne sont retéléchargées que si le
fichier `authorized_keys` n'existe pas.


### Création d'un utilisateur

Une règle primordiale dans l'administration, c'est de ne jamais ~~trop
travailler~~ utiliser le compte `root` directement ! quel que soit le sytème
d'exploitation.

Sur chaque machine, on crée donc autant de comptes utilisateurs, qu'il y aura
de personnes susceptibles de s'y connecter pour l'administrer[^whysudo].

[^whysudo]: non seulement cela évite d'exécuter n'importe quel script ou
    programme avec des privilèges lorsque ce n'est pas nécessaire, mais cela
    permet également de pouvoir retracer, via les journaux, qui a réalisé
    quelles opérations.

::::: {.exercice}

Utilisez `adduser(8)` pour vous créer votre propre compte, et au minimum
`passwd(1)` pour lui définir un mot de passe.

:::::

C'est bon, vous pouvez maintenant commencer à utiliser le client SSH de votre
choix pour administrer votre machine virtuelle !


### `su` -- Switch User

Mais comment faire des opérations d'administration, avec un simple compte
utilisateur ?

La commande `su -` permet de devenir `root`, moyennant la connaissance du mot
de passe du compte `root`. Notez l'option `-` passée à la commande, cela permet
de réinitialiser l'environnement comme si l'on venait de se connecter en tant
que l'utilisateur cible.

Afin de simplifier les opérations d'administration, une suite de commandes
fournies par le paquet `sudo` va devenir votre couteau suisse
d'administrateur. Utilisez :

- `sudo` : pour exécuter n'importe quelle commande, avec les privilèges
  administrateur ;
- `sudoedit` : pour éditer un fichier avec les droits administrateurs.

::::: {.exercice}

Après avoir installé le paquet `sudo`, vous devrez ajouter les utilisateurs
autorisés à se servir de `sudo` dans le groupe `sudo` ou `wheel` (en fonction
des distributions).

:::::

Vous pouvez également définir des conditions plus précises en éditant le
fichier `/etc/sudoers` (utilisez impérativement `visudo(1)` pour éditer ce
fichier).\

::::: {.code}

**Super astuce !** si vous oubliez de préfixer votre commande par `sudo`, vous
allez obtenir une erreur `Permission Denied`. Vous n'êtes pas obligés de
recopier la commande, utilisez :

<div lang="en-US">
```
sudo !!
```
</div>

:::::

### Top confort dans son coquillage

Un shell brut de configuration, c'est souvent source d'erreur ! N'hésitez pas à
recopier votre configuration et à changer votre shell (`chsh(1)`) pour vous
trouver dans un environnement plus amical !

On évitera toute configuration sur le compte `root`, étant donné qu'il est
susceptible d'être utilisé en dernier recours lorsque plus rien ne fonctionne,
une configuration basique aura plus de chance de résister. De toute manière,
vous n'utiliserez jamais ce compte !
