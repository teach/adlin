\newpage

Mieux utiliser Ansible
======================

Nous savons maintenant utiliser `ansible` et tirer parti des *playbooks* pour automatiser l'installation et la configuration de nos machines.

Nous avons vu toute la puissance des *playbooks* : en automatisant les tâches d'administration de nos machines, on réduit les risques d'erreurs humaines, tout en écartant les tâches répétitives de mise à jour, ... Néanmoins, nos *playbooks* peuvent devenir rapidement redondant (peut-être avez-vous géré l'ouverture des ports du pare-feu dans chaque *playbooks* : 80 et 443 dans le playbook `vitrine.yml` et 53 dans le *playbook* `nameserver.yml` ?).

Nous allons maintenant voir les roles Ansible qui résolvent ce problème en proposant un moyen standardisé et organisé de décomposer les tâches en composants plus petits et réutilisables.


Apprivoiser les rôles
---------------------

Les rôles Ansible sont un concept clef d'Ansible, conçus pour rationaliser et organiser nos configurations. Il s'agit d'avoir une approche modulaire : de la même manière que l'on essaie de factoriser les parties redondantes lorsque l'on programme une fonction, les rôles vont nous permettre de réutiliser des briques de configuration entre plusieurs *playbooks*.

Concrètement, les rôles Ansible regroupent un ensemble de tâches, *handlers*, variables, fichiers/modèles. En les concevant correctement, on peut aisément les partager entre différents projets.



### Structure d'un rôle


### Utilisation d'un rôle


###


Bonnes pratiques
================
