\newpage

Déploiement d'un service via Ansible
====================================

Très bien ! vous semblez avoir compris le fonctionnement de notre
gestionnaire de configuration.

Après cet échauffement, vous devriez être prêt à créer un *playbook*
dédié à l'installation d'un serveur [Matrix](https://matrix.org/) :

![Element : un des clients utilisable pour joindre son serveur Matrix](riot.png)


Vous connaissez et utilisez sans doute Discord ou Slack, un service de messagerie
instantannée qui a supplanté IRC, Jabber, ... Il vient avec le gros
inconvénient qu'il est centralisé, qu'il ne permet pas de faire discuter des
personnes qui, bien qu'utilisant le même service, ne sont pas dans le même
groupe ; mais surtout, il vient avec aucun élément de sécurité : il n'est en
effet pas possible d'avoir de chiffrement de bout-en-bout, entre deux personnes
ou un groupe, et les administrateurs des groupes, tout comme le gouvernement
américian, ont accès à tout l'historique. Vous l'aurez compris, Matrix est donc
une solution de messagerie décentralisée et permet de réaliser du chiffrement
de bout-en-bout.

Vous trouverez la documentation d'installation précise et détaillée à :
<https://matrix-org.github.io/synapse/latest/>.\

::::: {.warning}

Vous avez 5 solutions pour réaliser cette partie, mais 2 sont à proscrire. Vous pouvez :

- utiliser [le playbook officiel](https://github.com/spantaleev/matrix-docker-ansible-deploy) ;
- installer [les paquets officiels de la dernière version
  stable](https://matrix-org.github.io/synapse/latest/setup/installation.html#debianubuntu)
  et réaliser le playbook vous-même (quitte à recopier des bouts du playbook
  officiel) ;
- installer [les modules Python depuis PyPI](https://matrix-org.github.io/synapse/latest/setup/installation.html#installing-as-a-python-module-from-pypi).

Vous ne pouvez pas :

- déployer le conteneur Docker ;
- déployer à partir des sources.

:::::

Vous devrez attacher une importance tout particulière à la sécurité de
la solution que vous déployez : configurations durcies, utilisateur
dédié lorsque c'est possible (pas de `root` !), droits d'accès et
permissions des répertoires, etc.

Profitez des [modules de base de
données](https://docs.ansible.com/ansible/latest/collections/community/postgresql/index.html)
pour l'initialiser correctement. Et bien entendu de l'ensemble des
modules décrits dans la documentation standard !\

::::: {.warning}

**L'utilisation des modules
[`ansible.builtin.command`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html),
[`ansible.builtin.shell`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html),
[`ansible.builtin.raw`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/raw_module.html),
et assimilés est interdite en toute circonstance, sauf mention contraire explicite.**

:::::


## Configurations

Tous les éléments de configuration (mot de passes de connexion à la
base de données, chemins, etc.) sont à déclarer comme variables dans
le fichier `vars/matrix-config.yml`, que vous prendrez soin d'importer
dans votre *Playbook*.\

::::: {.warning}

Vous devez utiliser `ansible-vault` pour stocker de manière sûre les données
sensibles tels que les mots de passes. Vous pouvez ajouter un second fichier de
variables pour cela, si cela vous paraît plus opportun.

:::::

Vous devrez générer un certificat TLS afin de joindre les autres serveurs.\

::::: {.warning}

À aucun moment la clef privée de votre certificat ne doit faire partie de votre
dépôt. Dans certaines circonstances, il peut être pratique d'avoir cette clef
dans un *vault*, mais ici ce que l'on attend c'est que le certificat soit
généré s'il n'existe pas déjà ou qu'il a expiré. Éventuellement, une solution
automatisant son renouvellement automatique serait appréciable (mais non
obligatoire).

:::::


## Backup

Vous ajouterez enfin une tâche hebdomadaire de sauvegarder de la base
de données. Le *dump* obtenu est à placer dans `/var/backups/`.


## Client de test

Vous n'êtes pas tenu d'installer un client. Pour vos tests, vous pouvez
utiliser <https://app.element.io/>, en changeant l'adresse du serveur Matrix pour
votre sous-domaine dédié à Matrix (normalement
<https://matrix.login-x.srs.example.tld/>). (Conservez le serveur d'identité à
<https://vector.im>).



## Validation

Pour valider l'installation de votre serveur, rejoignez le canal
`#adlin:nemunai.re` et envoyez un message « Ping ! » pour signaler votre
présence.

Vous devriez également pouvoir tester en communiquant entre vous.
