---
title: Administration Linux avancée -- TP n^o^ 4
subtitle: Automatiser la gestion de configuration avec Ansible
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Jeudi 30 mars 2023
abstract: |
  Durant ce quatrième TP, nous allons apprendre à déployer des services sur un
  serveur, de manière industrielle !

  \vspace{1em}

  Ce TP contient un projet à rendre pour le **mercredi 12 avril à
  23 h 42**. Consultez la dernière partie pour plus d'informations.
...
