---
title: Administration Linux avancée -- Projet
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Jeudi 13 avril 2023
abstract: |
  Nous allons maintenant développer nos savoirs et connaissances
  d'Ansible en faisant le tour de quelques bonnes pratiques et en les
  appliquant à nos actuels playbooks.

  \vspace{1em}

  Ce projet est à rendre pour le
  **dimanche 30 avril 2023 à 23 h 42**.
...

\

En prenant appui sur la base des *playbooks* que vous avez commencés durant les précédents TP, vous devez fiabiliser votre travail et appliquer un certain nombre de [bonnes pratiques courantes](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html).\


Éléments supplémentaires
========================

Par rapport au sujet précédent, vous devez :

* Faire usage des [rôles Ansible](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html) autant que possible : que ce soit les votres dans un dossier `roles` ou la [communauté](https://galaxy.ansible.com/). À ce stade, sont au moins attendu sous forme de rôle : le pare-feu, l'installation et la configuration des serveurs installés, la gestion des certificat (idéalement vos *playbook* ne devraient qu'appeler des rôles, avec les bonnes variables).\

* Le *playbook* `name-server.yml` doit également installer et configurer [happyDomain](https://www.happydomain.org/fr/) au moyen de la [collection Ansible mise à disposition par le projet](https://galaxy.ansible.com/happydns/happydomain) (sans Docker, en écoute sur tous les ports 8081, selon la configuration par défaut, pensez à ouvrir le port du pare-feu).\

* Le contenu de votre clef SSH publique doit se trouver dans la variable `ssh_key`. Cette variable doit avoir une précédence lui permettant de pouvoir être écrasée par une surcharge sur la ligne de commande (`ansible-playbook -e ssh_key="ssh-ed25519 ABCDEF..."`). Votre clef DOIT bien être présente dans une variable et être utilisée si la variable n'est pas précisée sur la ligne de commande.\

* De même, une variable `acme_directory` doit pouvoir vous permettre de passer facilement de l'infrastructure de production (par défaut), à [l'environnement `staging`](https://letsencrypt.org/docs/staging-environment/) de votre fournisseur de certificats. (Vous devez impérativement utiliser l'environnement `staging` durant vos tests.) Le contenu par défaut est : `https://acme-v02.api.letsencrypt.org/directory`\

* Utiliser le secret `4dL1n!` pour chiffrer le contenu de vos [`ansible-vault`](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html).\

* [KICS](https://docs.kics.io/latest/getting-started/) ne doit pas trouver de problème significatif.\

* En bonus, utilisez `happyDomain` (au travers des modules disponibles) pour créer le sous-domaine de votre vitrine.\

* N'oubliez pas de relire les guides de l'ANSSI pour vérifier que votre *playbook* `securing.yml` prend en compte un maximum de remarques pertinentes.\


Arborescence attendue
========

Tous les fichiers identifiés comme étant à rendre sont à placer dans un dépôt
Git privé, que vous partagerez avec [votre
professeur](https://gitlab.cri.epita.fr/nemunaire/).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires,
cela dépendra de votre avancée dans le projet) :

<div lang="en-US">
```
./basis.yml
./securing.yml
./vitrine.yml
./name-server.yml
./collections/requirements.yml
./roles/requirements.yml
...
```
</div>

Votre rendu sera pris en compte en faisant un [tag **signé par votre clef
PGP**](https://lessons.nemunai.re/keys) ou SSH. Consultez les détails du rendu
(nom du tag, ...) sur la page dédiée au projet sur la plateforme de rendu.

Les fichiers `requirements.yml` sont standardisés : [pour les collections](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#install-multiple-collections-with-a-requirements-file) et [pour les rôles](https://galaxy.ansible.com/docs/using/installing.html#installing-multiple-roles-from-a-file), placez-y toutes vos dépendances.
