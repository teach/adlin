Recommandations
---------------

Nouvelle page de publicité, l'ANSSI publie également un guide de bonnes
pratiques sur l'aquisition et l'exploitation de noms de domaines :
<https://www.ssi.gouv.fr/uploads/2014/05/guide_dns_fr_anssi_1.3.pdf>

![Guide de recommandations pour les noms de domaines](guide_dns_fr_anssi_1.3-couverture.png){width=20%}

Vous n'avez pas de recommandations à tirer de ce guide, mais dans le cas où
vous auriez la responsabilité d'un domaine, c'est un guide à prendre en compte.


### DNSSEC (bonus)

Le DNS est un vieux protocole, particulièrement important sur Internet. Il
souffre malheureusement de son âge, en particulier de part le manque de
plusieurs mécanismes de sécurité. Citons notamment le fait que les paquets
transitent en clair, souvent via UDP, sans mécanisme d'authentification ni de
signature.

Pour améliorer la situation, DNSSEC est une extension du protocole DNS qui
permet de publier des signatures pour chaque enregistrement.

Pour ce faire, et toujours parce que l'on se trouve dans le cadre d'une
structure arborescente, les clefs publiques permettant de valider les
enregistrements d'un domaine en particulier, sont à publier dans la zone
parente. C'est pourquoi, vous disposez sur Maatma, d'une interface vous
permettant d'indiquer vos clefs publiques.

::::: {.exercice}

Vous devriez sécuriser les réponses envoyées par votre serveur DNS en
configurant DNSSEC.

De nombreux articles sont disponibles sur Internet pour vous permettre de
configurer DNSSEC en fonction du serveur faisant autorité que vous aurez
choisi.

Prenez garde, les signatures doivent être regénérées régulièrement. Ne
choisissez pas une méthode ancestrale qui n'utiliserait pas les fonctionnalités
de signature autonome du programme !

:::::
