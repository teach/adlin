\newpage

Délégation de nom de domaine
============================

Ça y est, votre vitrine est sécurisée ! Néanmoins, nous avons un peu
triché car vous ne contrôliez pas le nom de domaine de votre
vitrine. C'était Maatma qui vous fournissait un domaine, un peu comme
si vous créiez un blog Wordpress et utilisiez un domaine
xxxx.wordpress.com fourni par le gestionnaire du service.

Nous allons maintenant gérer notre propre domaine.\

La deuxième solution proposée par Maatma est une délégation de nom de domaine.
Alors que jusque-là, c'étaient les serveurs DNS de Maatma qui répondaient aux
requêtes DNS, avec la délégation, ce sera à vous, dans votre VM, de répondre
aux requêtes venues tout droit d'Internet.

Voici les grandes étapes :

* choisir un serveur DNS autoritaire (BIND, NSD, Knot, PowerDNS, djbDNS, ...) ;
* éditer la zone de votre domaine ;
* configurer ce serveur pour qu'il réponde effectivement aux requêtes de votre zone ;
* tester avec `dig @votreIPv4locale ANY login-x.srs.example.tld` que votre serveur réagit bien correctement ;
* publier sur Maatma le nom de domaine où votre serveur de nom est joignable (généralement `ns.login-x.srs.example.tld`), il s'agit d'un enregistrement `NS` ;
* publier également sa *GLUE*, afin de résoudre le problème de poule et d'œuf (le domaine de votre serveur de noms étant dans la zone qu'il est censé servir, le bureau d'enregistrement enverra également l'IP correspondant au domaine) ;
* tester avec `dig @9.9.9.9 AAAA login-x.srs.example.tld` que votre serveur est bien joignable.

Un ouvrage de référence, qui répondra à l'intégralité de vos questions et manières d'utiliser votre serveur DNS se trouve à <http://www.zytrax.com/books/dns/>.


## Coller aux spécifications

Pourquoi une telle complexité apparente pour déléguer une zone DNS ?

Vous êtes sur le point d'obtenir le contrôle total d'un domaine :
`login-x.srs.example.tld.`. Cela signifie que vous allez pouvoir décider,
depuis votre propre serveur de noms (respectueux des
[standards](https://www.ietf.org/rfc/rfc1034.txt)), comment vous allez
répondre aux serveurs résolveurs des utilisateurs.

Le protocole DNS étant décentralisé, mais basé sur une arborescence
unique, il est nécessaire que les serveurs faisant autorité sur une
zone (`fr.`, `srs.example.tld.`, ...) fournissent toutes les informations
nécessaires pour que cette délégation fonctionne.

À cet instant, vous connaissez l'adresse IPv6 routable sur Internet de votre
serveur de noms (comme vous n'avez aujourd'hui qu'une seule machine, c'est
l'adresse de votre tunnel). Il va donc falloir indiquer à Internet que c'est
cette IP qu'il faut contacter, si l'on veut interroger le serveur faisant
autorité.

Concrètement, vous allez devoir vous attribuer un sous-domaine pour votre
serveur de noms, car c'est grâce à un enregistrement `NS` figurant dans la zone
parente (pour vous, la zone parente c'est `srs.example.tld`) que la délégation va se
faire.

Dans le cas basique, on va utiliser les serveurs de quelqu'un d'autre (d'un
hébergement spécialisé par exemple, ou celui de son bureau d'enregistrement
s'ils proposent ce service). Auquel cas, on fera figurer ce genre
d'informations :

<div lang="en-US">
```
42sh$ dig @e.ext.nic.fr. NS epita.fr.
;; ->>HEADER<<- opcode: QUERY; status: NOERROR; id: 17590
;; Flags: qr rd; QUERY: 1; ANSWER: 0; AUTHORITY: 3; ADDITIONAL: 0

;; QUESTION SECTION:
;; epita.fr.                    IN      NS

;; AUTHORITY SECTION:
epita.fr.               172800  IN      NS      tooty.ionis-it.com.
epita.fr.               172800  IN      NS      kazooie.ionis-it.com.
epita.fr.               172800  IN      NS      banjo.ionis-it.com.

;; Received 100 B
;; Time 2042-12-04 13:42:23 CET
;; From 2a00:d78:0:102:193:176:144:22@53(UDP) in 13.9 ms
```
</div>

La commande est envoyée spécifiquement aux serveurs de l'AFNIC, faisant
autorité pour la zone `fr.`. Les serveurs de l'AFNIC nous indiquent en retour,
que `epita.fr.` est déléguée à Ionis, et qu'elle dispose de 3 serveurs faisant
autorité.

Mais dans votre cas, vous hébergez vous-même votre propre serveur au sein de
votre zone, vous n'avez pas d'autre domaine à votre disposition. C'est
également le cas de Wikipédia :

<div lang="en-US">
```
42sh$ dig @d0.org.afilias-nst.org. NS wikipedia.org.
;; ->>HEADER<<- opcode: QUERY; status: NOERROR; id: 47396
;; Flags: qr rd; QUERY: 1; ANSWER: 0; AUTHORITY: 3; ADDITIONAL: 3

;; QUESTION SECTION:
;; wikipedia.org.               IN      NS

;; AUTHORITY SECTION:
wikipedia.org.          86400   IN      NS      ns0.wikimedia.org.
wikipedia.org.          86400   IN      NS      ns2.wikimedia.org.
wikipedia.org.          86400   IN      NS      ns1.wikimedia.org.

[...]
```
</div>

On voit ici que pour résoudre les sous-domaines de `wikipedia.org.`, il faut
demander à `nsX.wikimedia.org.`. Mais comment obtenir alors, l'adresse de ces
serveurs de noms, puisque l'on ne sait pas où les contacter ...!

C'est là que les *GLUE records* entrent en jeu !

J'ai volontairement tronqué la sortie de la commande précédente, en entier, c'est :

<div lang="en-US">
```
42sh$ dig @d0.org.afilias-nst.org. NS wikipedia.org.
;; ->>HEADER<<- opcode: QUERY; status: NOERROR; id: 47396
;; Flags: qr rd; QUERY: 1; ANSWER: 0; AUTHORITY: 3; ADDITIONAL: 3

;; QUESTION SECTION:
;; wikipedia.org.               IN      NS

;; AUTHORITY SECTION:
wikipedia.org.          86400   IN      NS      ns0.wikimedia.org.
wikipedia.org.          86400   IN      NS      ns2.wikimedia.org.
wikipedia.org.          86400   IN      NS      ns1.wikimedia.org.

;; ADDITIONAL SECTION:
ns0.wikimedia.org.      86400   IN      A       208.80.154.238
ns1.wikimedia.org.      86400   IN      A       208.80.153.231
ns2.wikimedia.org.      86400   IN      A       91.198.174.239

;; Received 143 B
;; Time 2042-12-04 13:42:23 CET
;; From 2001:500:f::1@53(UDP) in 15.2 ms
```
</div>

Afin d'éviter de tourner en rond sans jamais avoir réponse à notre question, en
même temps que de nous répondre sur qui sont les serveurs faisant autorité pour
la zone `wikipedia.org.`, les serveurs gérant la zone `org.` indiquent
également à quelle adresse ont peut les contacter.

Ce sont les administrateurs de la zone `wikipedia.org.` qui ont indiqué à
`org.` quels étaient leurs serveurs de noms. Et ils ont également donné des
*GLUE records*, pour permettre à la magie d'opérer.

::::: {.exercice}

À vous maintenant de créer votre zone, en envoyant sur Maatma, le nom de
domaine votre serveur de noms, ainsi que le *GLUE record* qui lui correspond.

:::::

::::: {.warning}

Rappelez-vous que vous ne disposez que d'une plage IPv6 publique. La
connectivité IPv4 est fournie par Maatma, lorsque les protocoles
permettent de router les paquets entrant.

Bien qu'il serait théoriquement possible de le faire pour le DNS,
**Maatma n'assure pas la résolution de vos noms de domaines**.

Lorsque vous définissez votre *GLUE record*, vous ne devez indiquer
que l'adresse IPv6 de votre serveur faisant autorité. **N'indiquez pas
l'IPv4 de Maatma pour le sous-domaine dédié au serveur de noms.**

Même si vous êtes dans un réseau entièrement IPv4, le résolveur de ce
réseau va *généralement*[^NOLOCALIPv6] transmettre les requêtes à un
résolveur qui aura une connectivité IPv6.

[^NOLOCALIPv6]: Un résolveur pour une entité peut faire le choix de
    résoudre lui-même les noms de domaines, et de ne pas être relié en
    IPv6. Néanmoins vous ne serez pas confronté à ce cas de figure sur
    les réseaux que vous utiliserez (EPITA, FAI français, ...). Si
    vous l'êtes tout de même, changez l'adresse de votre résolveur
    pour utiliser un résolveur public tel que celui de
    [FDN](https://www.fdn.fr/actions/dns/) ou
    [d'autres](https://en.wikipedia.org/wiki/Public_recursive_name_server).

:::::


## À propos de Maatma et du portail IPv4

Maatma met à votre disposition une plage d'IPv6 publique, directement
routable sur Internet. Mais aujourd'hui encore, de nombreuses
installations ne disposent pas de ce protocole de communication et ne
peuvent accéder aux machines connectées en IPv4 seulement.

Pour pallier cela, Maatma met à votre disposition une IPv4 mutualisée
entre tous les SRS. Un service écoute sur la machine au bout de cette
IP, afin de distribuer le trafic entre vous, selon la demande du
client. Tous les protocoles ne permettent pas d'identifier le domaine
de destination, donc ce sont les protocoles HTTP et HTTPS qui sont
transmis sur vos IPv6.
