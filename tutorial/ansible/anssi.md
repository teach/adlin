Durcir les configurations de base
---------------------------------

Le pare-feu va vous aider à être moins susceptible d'attirer l'attention en
présentant des scans épurés, et en exposant exclusivement que les ports
nécessaires, aux bonnes personnes. Mais vous allez sans doute devoir présenter
un certain nombre de services à Internet. Il convient donc de sécuriser au
mieux ces services.

Avant même de regarder service par service, il est important de passer en revue
la configuration du système d'exploitation en lui-même.

Un bon point de départ pour faire cela est de suivre les recommandations de
l'ANSSI que vous jugez adaptées à votre déploiement :\
<https://www.ssi.gouv.fr/guide/recommandations-de-securite-relatives-a-un-systeme-gnulinux/>

![Le guide de recommandations de l'ANSSI fait référence](linux_configuration-fr.jpg){width=30%}

::::: {.question}

#### Quel niveau de durcissement choisir ? {-}

Vous devriez viser un niveau de durcissement **minimal** selon la
classification de l'ANSSI.

Par ailleurs, les recommandations 1 à 8 et 28 ne sont pas applicables ici puisque
vous êtes dans une machine virtuelle, et le bootloader est figé sur l'ISO. Mais
peut-être que vous devriez songer à appliquer ces recommandations à vos propres
machines.

:::::

::::: {.exercice}

Faites le tour des configurations afin de vous assurer qu'elles sont
suffisamment durcies. Pensez à noter vos changements, vous devrez les
reporter dans un fichier au prochain TP !

:::::

Outre la difficulté supplémentaire qu'un attaquant aura à pénétrer un système
correctement configuré, les scans automatiques auront également moins
d'informations à afficher car la récolte d'empreinte sera d'autant plus
difficile (par exemple si un logiciel n'expose pas son numéro de version).
