\newpage

Déploiement d'un second service web
===================================

NextCloud
---------

- Utilisation d'un rôle existant (ou création)
- Aïe besoin d'un nom de domaine


Noms de domaines
----------------

- Utilisation d'un rôle pour avoir un serveur DNS autoritaire (bind)


### Rappels sur le DNS

### Mise en place du résolveur

### Mise en place du serveur autoritaire

### Test d'une zone
