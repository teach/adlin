\newpage

Rendu
=====

Arborescence attendue
-------

Tous les fichiers identifiés comme étant à rendre sont à placer dans un dépôt
Git privé, que vous partagerez avec [votre
professeur](https://gitlab.cri.epita.fr/nemunaire/).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires,
cela dépendra de votre avancée dans le projet) :

<div lang="en-US">
```
./basis.yml
./securing.yml
./vitrine.yml
./name-server.yml
...
```
</div>

Votre rendu sera pris en compte en faisant un [tag **signé par votre clef
PGP**](https://lessons.nemunai.re/keys). Consultez les détails du rendu (nom du
tag, ...) sur la page dédiée au projet sur la plateforme de rendu.


Quelques conseils
-----

* Le fichier `hosts` avec votre inventaire sera écrasé. Vous pouvez le rendre ou non.

* Tous les `playbooks` doivent pouvoir être exécutés plusieurs fois. Exécuté deux fois de suite, un même playbook ne doit pas trouver de changement.

* Pour simplifier les choses, le *playbook* `basis.yml` sera systématiquement appelé avant tous les autres et le *playbook* `name-server.yml` sera toujours appelé avant `vitrine.yml`. Les autres doivent pouvoir s'exécuter dans un ordre indéfini.
