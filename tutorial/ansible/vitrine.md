Vitrine
=======

Ma première vitrine
-------------------

Sur Maatma, [vous avez pu demander un
sous-domaine](https://adlin.nemunai.re/maatma/domains)/*Association simple*,
qui vous a attribué un nom de domaine (ou vous avez pu choisir d'utiliser un
sous-domaine de votre propre nom de domaine).

::::: {.exercice}

L'idée maintenant, ce sera de déployer sur le domaine
`login-x.adlin2024.example.tld`, une vitrine d'entreprise basique. Vous n'avez
pas nécessairement besoin de déployer tout un Wordpress : un simple lot de
pages HTML ... générées avec Hugo, fera l'affaire.

:::::

Vous aurez pour cela besoin d'un serveur web, dont le choix est laissé à votre
discrétion.

Une fois votre serveur web configuré et votre vitrine déployée, accédez à votre
domaine depuis votre poste pour constater la bonne marche de l'installation
(vous aurez sans doute à adapter les règles de votre pare-feu).


Vitrine sécurisée
-----------------

Dès que l'on déploie un nouveau service, qui plus est à destination de tout
Internet, comme c'est le cas ici pour notre vitrine, il convient de faire
particulièrement attention à la sécurité du service. Même dans le cas d'une
simple vitrine, de nombreux éléments de configuration peuvent rendre
l'installation sujète à des attaques.

Dans le cas d'un site web, nous retrouvons un guide de l'ANSSI dédié :\
<https://www.ssi.gouv.fr/uploads/2013/05/anssi-guide-recommandations_mise_en_oeuvre_site_web_maitriser_standards_securite_cote_navigateur-v2.0.pdf>

![Le guide de recommandations de l'ANSSI pour les sites web](anssi-guide-recommandations_mise_en_oeuvre_site_web-couverture.png){width=22%}

::::: {.exercice}

Le guide est une nouvelle fois fort passionnant, mais nous nous concentrerons
aujourd'hui exclusivement sur la recommandation R1 : *Mettre en œuvre TLS à
l'état de l'art*.

Entre autre, il s'agit déjà de disposer d'un certificat valide pour votre
vitrine.

:::::

Vous pouvez utiliser les services de [Let's Encrypt](https://letsencrypt.org/)
pour obtenir un certificat TLS.\

::::: {.warning}

Compte tenu [des limitations
imposées](https://letsencrypt.org/docs/rate-limits/), vous ne pourrez pas tous
en créer un aujourd'hui, mais n'hésitez pas à retenter un peu plus tard dans la
semaine. Vous pouvez également obtenir vos certificats depuis de nombreux
autres services gratuits similaires : [ZeroSSL](https://zerossl.com/),
[buypass](https://www.buypass.com/), ...\

D'ailleurs, si vous disposez de votre propre nom de domaine et que vous
souhaitez l'utiliser pour ce TP, vous pouvez suivre les instructions dans
l'interface de Maatma pour pouvoir l'utiliser.

:::::

Let's Encrypt est une entité qui génère de manière automatisée et gratuitement
des certificats TLS, dans le but de sécuriser l'accès aux services d'un domaine
(que ce soit pour utiliser HTTPS, SMTPS, FTPS, ...).

Leur service repose sur un test de propriété du domaine, qui peut être réalisé
de différentes manières (`http-01`, `dns-01`, ...). Une fois le test de
propriété validé, il est possible de faire émettre un certificat pour une durée
de 90 jours, pour le domaine validé.

Ce court délai permet d'éviter d'avoir trop de révocations, mais cela implique
aux administrateurs d'automatiser la gestion des certificats (par exemple via
une tâche *cron* qui va régulièrement venir renouveler les certificats et
relancer les services qui les utilisent).

De nombreux programmes sont disponibles pour exploiter le [protocole
ACME](https://www.rfc-editor.org/rfc/rfc8555) : citons notamment
[`certbot`](https://eff-certbot.readthedocs.io/en/stable/index.html)
(implémentation officielle, mais dont l'installation et l'usage tendent à se
complexifier même dans le cas des installations les plus simples), ou
[`dehydrated`](https://github.com/dehydrated-io/dehydrated) (qui fournit un
script dont le seul but est de générer un certificat sur une installation
existante, avec moins de dépendances à l'installation). Mais de nombreux autres
projets sont tout aussi viables pour utiliser le protocole ACME.
