---
title: Administration Linux avancée -- TP n^o^ 5
subtitle: Bonnes pratiques d'Ansible
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Jeudi 13 avril 2023
abstract: |
  Durant ce cinquième TP, nous allons développer nos savoirs et connaissances
  d'Ansible en faisant le tour de quelques bonnes pratiques et en les
  appliquant à nos actuels playbooks.

  \vspace{1em}

  Ce TP est le projet final du cours qui sera à rendre pour le
  **dimanche 30 avril 2023 à 23 h 42**.
...
