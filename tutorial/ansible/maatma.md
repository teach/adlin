\newpage

Maatma
======

Maatma sera votre bureau d'enregistrement (*registrar*) et votre fournisseur
d'accès à une plage d'IPv6 routable sur Internet.\

::::: {.warning}

Ce service vous est proposé à titre éducatif. Vous savez
combien Internet peut être un milieu hostile, il est donc de votre devoir de ne
pas tenter le diable et de prendre toutes les mesures qui s'imposent pour vous
protéger en conséquence. D'autre part, il va de soi que ce service vous est
fourni en échange de **votre consentement à ne pas l'utiliser d'une manière
condamnable** (mais libre à vous de tester la plate-forme en elle-même).

:::::

L'interface de Maatma est accessible à cette adresse :
<https://adlin.nemunai.re/maatma/>. Utilisez votre identifiant CRI pour vous y
connecter.


Présentation du système d'information
-------------------------------------

Le système d'information que vous allez avoir à gérer aujourd'hui est très
restreint, car il ne se compose que d'une seule machine !

Cette semaine, nous allons installer et configurer plusieurs services sur cette
machine. Pour rendre les choses un peu plus attrayantes, une fois démarrée,
votre machine est automatiquement accessible depuis Internet.

L'image ISO que vous avez récupérée met à votre disposition un système Debian
minimaliste. Cette image s'occupe de faire le nécessaire pour démarrer le
système, sans que vous ayez à réaliser une installation complète, qui est plus
longue qu'intéressante. Néanmoins, si vous avez des interrogations au sujet de
la manière dont une installation se fait, ou du fonctionnement du bootloader,
n'hésitez pas à poser vos questions !


Tunnel IPv6
-----------

Au premier lancement de votre VM, la machine vous demandera d'indiquer un jeton
afin de mettre en place le tunnel IPv6.

Afin d'en obtenir un, rendez-vous sur la [page
Tunnels](https://adlin.nemunai.re/maatma/tunnels) et créez un nouveau
tunnel. Un jeton de 10 caractères s'affichera alors, c'est celui que
vous devrez recopier dans le terminal (attention à la casse !).


### Test du tunnel

L'interface `wg0` correspond à la porte d'entrée du trafic Internet. C'est elle
que vous devez garder avec attention.

Votre passerelle répond aux `ping`, une fois la connexion établie, vous devriez
pouvoir :

```
ping 2a01:e0a:518:833::1
```


IPs et domaine de test
----------------------

### Les IP

En plus de vous fournir une IPv6 publique, vous disposerez d'un sous-domaine
propre ainsi qu'une délégation sur un domaine.

Notez que vous **ne disposez pas** d'IPv4 publique (c'est-à-dire routable sur
Internet), vous disposez seulement d'une IPv6 publique. L'ensemble de vos
services seront cependant accessibles également en IPv4, car Maatma transmet
les requêtes faites en IPv4 et les distribue entre vos IPv6, **lorsque le
protocole permet de faire cette redistribution**. Par exemple, HTTP et HTTPS le
permettent, mais pas SSH. Vous ne pourrez donc pas vous connecter en SSH via
l'IPv4 de Maatma (ni en utilisant votre domaine).

Voici un schéma reprenant ces explications :

![Schéma du tunnel mis en place par Maatma](maatma-tun.png)

On voit bien que l'IPv6 est connectée directement au tunnel, mais que l'IPv4
passe par une étape de sélection. Les protocoles qui exposent le domaine de
destinations tels que HTTP et HTTPS sont donc routés vers vos machines
virtuelles respectives.

### Les domaines

Dans l'interface de Maatma, vous avez la possibilité de demander un simple
sous-domaine (*Association simple*) : ceci créera un domaine qui vous permettra
d'accéder à votre machine sans avoir à connaître son IP. Avec l'*Association
simple* Maatma gère le serveur de noms, vous n'avez rien à faire.

Dans un deuxième temps, nous verrons comment tirer parti de la délégation. Vous
pouvez l'ignorer pour le moment.\


::::: {.question}

##### J'ai un nom de domaine perso, puis-je l'utiliser ? {-}

Oui ! Que ce soit pour l'association ou pour la délégation, un bouton est
disponible pour vous permettre de dédier un sous-domaine de votre domaine
personnel. Vous aurez à faire quelques configurations dans votre zone DNS, mais
tout est expliqué clairement dans la boîte de dialogue.

:::::
