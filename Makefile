LINUXKIT ?= $(GOPATH)/bin/linuxkit

tuto1: login-initrd.img challenge token-validator/token-validator server.iso

pkg/login-app/cmd/login-app: pkg/login-app

pkg/login-app: pkg/login-app/cmd/login.go pkg/login-app/cmd/dialog-checklogin.go pkg/login-app/cmd/dialog-login.go pkg/login-app/cmd/login-app pkg/login-app/cmd/dialog-errmsg.go pkg/login-app/cmd/main.go pkg/login-app/cmd/stream.go pkg/login-app/cmd/cinematic.go pkg/login-app/build.yml pkg/login-app/Dockerfile
	$(LINUXKIT) pkg build pkg/login-app/
	#$(LINUXKIT) pkg push --sign=false pkg/login-app/
	touch pkg/login-app

login-initrd.img: login.yml pkg/login-app
	$(LINUXKIT) build $<

token-validator/token-validator: token-validator/*.go
	go generate ./token-validator
	GOOS=linux GOARM=5 GOARCH=arm go build -tags netgo -ldflags '-w -extldflags "-static"' -o $@ ./token-validator

pkg/challenge: pkg/challenge/adlin pkg/challenge/issue pkg/challenge/init pkg/challenge/ssl/ec_cert.pem
	$(LINUXKIT) pkg build pkg/challenge/

subject/adlin.6.gz: subject/adlin.6.md
	make -C subject adlin.6.gz

challenge-initrd.img: challenge.yml subject/adlin.6.gz subject/adlin-TP1-topologie.png pkg/challenge pkg/challenge/init pkg/shadow-up
	$(LINUXKIT) build $<


pkg/arp-spoofer: pkg/arp-spoofer/cmd/main.go pkg/arp-spoofer/cmd/arp.go pkg/arp-spoofer/build.yml pkg/arp-spoofer/Dockerfile
	$(LINUXKIT) pkg build $@
	touch $@

pkg/chrony: pkg/chrony/build.yml pkg/chrony/Dockerfile
	$(LINUXKIT) pkg build pkg/chrony/
	touch pkg/chrony

pkg/shadow-up: pkg/shadow-up/build.yml pkg/shadow-up/Dockerfile
	$(LINUXKIT) pkg build pkg/shadow-up/
	touch pkg/shadow-up

pkg/login-validator: pkg/login-validator/cmd/login.go pkg/login-validator/cmd/main.go pkg/login-validator/cmd/pxetpl.go pkg/login-validator/cmd/logout.go pkg/login-validator/cmd/auth.go pkg/login-validator/cmd/arp.go pkg/login-validator/cmd/auth_krb5.go pkg/login-validator/cmd/auth_ldap.go pkg/login-validator/cmd/students.go pkg/login-validator/cmd/auth_fwd.go pkg/login-validator/cmd/ssh.go pkg/login-validator/build.yml pkg/login-validator/Dockerfile
	$(LINUXKIT) pkg build pkg/login-validator/
	touch pkg/login-validator

pkg/minichecker: pkg/minichecker/build.yml pkg/minichecker/cmd/main.go pkg/minichecker/cmd/adlin.token pkg/minichecker/cmd/adlin.conf pkg/minichecker/cmd/checker.go pkg/minichecker/cmd/encode.go pkg/minichecker/cmd/wg.go pkg/minichecker/Dockerfile
	$(LINUXKIT) pkg build pkg/minichecker/
	touch pkg/minichecker

pkg/resolver: pkg/resolver/build.yml pkg/resolver/docker-entrypoint.sh pkg/resolver/Dockerfile
	$(LINUXKIT) pkg build pkg/resolver/
	touch pkg/resolver

pkg/monit: pkg/monit/build.yml pkg/monit/Dockerfile
	$(LINUXKIT) pkg build pkg/monit/
	touch pkg/monit

pkg/postfix: pkg/postfix/build.yml pkg/postfix/docker-entrypoint.sh pkg/postfix/Dockerfile
	$(LINUXKIT) pkg build pkg/postfix/
	touch pkg/postfix

pkg/tftpd: pkg/tftpd/build.yml pkg/tftpd/Dockerfile
	$(LINUXKIT) pkg build pkg/tftpd/
	touch pkg/tftpd

pkg/unbound: pkg/unbound/build.yml pkg/unbound/docker-entrypoint.sh pkg/unbound/Dockerfile
	$(LINUXKIT) pkg build pkg/unbound/
	touch pkg/unbound

pkg/wg-manager: pkg/wg-manager/cmd/register.go pkg/wg-manager/cmd/main.go pkg/wg-manager/build.yml pkg/wg-manager/Dockerfile
	$(LINUXKIT) pkg build pkg/wg-manager/
	touch pkg/wg-manager

server.iso: server.yml students.csv solver.sh ssl/fullchain.pem ssl/privkey.pem challenge-initrd.img pkg/arp-spoofer pkg/chrony pkg/login-validator pkg/monit pkg/postfix pkg/tftpd pkg/unbound pkg/wg-manager challenge-kernel login-initrd.img
	$(LINUXKIT) build -format iso-bios $<

pkg/debian-tuto2: pkg/debian-tuto2/sshd_config pkg/debian-tuto2/gai.conf pkg/debian-tuto2/isolinux.cfg pkg/debian-tuto2/build.yml pkg/debian-tuto2/default.script pkg/debian-tuto2/issue pkg/debian-tuto2/Dockerfile
	$(LINUXKIT) pkg build pkg/debian-tuto2/
	touch pkg/debian-tuto2

pkg/debian-tuto3: pkg/debian-tuto3/sshd_config pkg/debian-tuto3/build.yml pkg/debian-tuto3/issue pkg/debian-tuto3/Dockerfile
	$(LINUXKIT) pkg build pkg/debian-tuto3/
	touch pkg/debian-tuto3

pkg/router-tuto3: pkg/router-tuto3/build.yml pkg/router-tuto3/Dockerfile
	$(LINUXKIT) pkg build pkg/router-tuto3/
	touch pkg/router-tuto3

pkg/tinydeb: pkg/tinydeb/sshd_config pkg/tinydeb/gai.conf pkg/tinydeb/build.yml pkg/tinydeb/Dockerfile
	$(LINUXKIT) pkg build pkg/tinydeb/
	touch pkg/tinydeb

pkg/nsd: pkg/nsd/sshd_config pkg/nsd/build.yml pkg/nsd/init pkg/nsd/Dockerfile
	$(LINUXKIT) pkg build pkg/nsd/
	touch pkg/nsd

tuto2-kernel: tuto2.yml
	$(LINUXKIT) build $<
tuto2-initrd.img: tuto2.yml
	$(LINUXKIT) build $<
tuto2-cmdline: tuto2.yml
	$(LINUXKIT) build $<

tuto2.iso: tuto2.yml pkg/debian-tuto2 tuto2-kernel tuto2-initrd.img tuto2-cmdline
	$(LINUXKIT) build -format iso-bios $<

tuto2-srs.iso: tuto2.iso pkg/debian-tuto2/isolinux.cfg
	$(eval TDIR := $(shell mktemp -d))
	sudo bsdtar xf $< -C $(TDIR)
	sudo cp pkg/debian-tuto2/isolinux.cfg /usr/share/syslinux/isolinux.bin /usr/share/syslinux/ldlinux.c32 /usr/share/syslinux/vesamenu.c32 /usr/share/syslinux/menu.c32 /usr/share/syslinux/libcom32.c32 /usr/share/syslinux/libutil.c32 /usr/share/syslinux/poweroff.c32 $(TDIR)/isolinux/
	$(eval CMDLINE := $(shell cat tuto2-cmdline | sed 's/console=ttyS0 //;s#root=/dev/sr0 ##;s#root=/dev/sda1 ##;s#adlin.format=/dev/sda ##;'))
	sudo sed -i 's#<CMDLINE>#$(CMDLINE)#' $(TDIR)/isolinux/isolinux.cfg
	sudo mkisofs -o $@ -l -J -R -c isolinux/boot.cat -b isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -joliet-long -input-charset utf8 -V AdLin2 $(TDIR)
	sudo chown 1000 $@
	sudo rm -rf $(TDIR)
	isohybrid $@


tuto3-kernel: tuto3.yml
	$(LINUXKIT) build $<
tuto3-initrd.img: tuto3.yml
	$(LINUXKIT) build $<
tuto3-cmdline: tuto3.yml
	$(LINUXKIT) build $<

tuto3.iso: tuto3.yml pkg/debian-tuto3 pkg/router-tuto3 pkg/minichecker pkg/tinydeb pkg/resolver pkg/nsd
	$(LINUXKIT) build -format iso-bios $<


%.gz: %
	gzip -9 < $< > $@

%.torrent: %
	mktorrent -o $@ -a http://ankh.serekh.nemunai.re:6969/announce -p -v ./$<
