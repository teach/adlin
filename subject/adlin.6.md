---
title: ADLIN
section: 6
header: ADvanced LINux administration
footer: EPITA SRS 2024
author: Écrit par Pierre-Olivier *nemunaire* Mercier <**nemunaire+adlin@nemunai.re**>
date: 2023-02-22
...

# NOM

ADLIN - Travaux pratiques d'ADministration système LINux avancée


# SYNOPSIS

Dans un environnement hostile, vous devez vous infiltrer au sein d'un système
et réussir à en reprendre le contrôle, alors qu’il n’y a plus d’infrastructure
réseau, ni de système d’automatisation à votre disposition.


# ENVIRONNEMENT

Mettez-vous à l'aise ! Vous risquez d'être bloqué dans ce TTY pour un moment,
alors prenez quelques minutes pour le mettre à votre goût : **loadkeys**(1),
**loadfonts**(1), ...


# DESCRIPTION

Pour chacune des étapes à suivre, vous devez envoyer un jeton à l'URL indiquée.

Le programme **adlin** peut vous aider à envoyer le jeton au bon format et à y
inclure les données demandées par le serveur de validation.

Il n'y a pas de limite sur le nombre de tentatives que vous pouvez effectuer ;
mais essayez tout de même de comprendre ce que vous faites !


## STAGE -1

Afin de pouvoir accéder au TP, vous devez commencer par vous authentifier :
cela permet d'assurer le suivi de votre progression. Votre nom d'utilisateur et
votre mot de passe CRI vous seront nécessaire.

Au moment de votre authentification, le système vous aura attribué une IP qui
vous est dédiée. Vous êtes libre d'utiliser n'importe quelle autre IP de la
plage qui ne serait pas attribuée.


## STAGE 0

Outrepasser l'écran de connexion et devenir **root** sur le système.


## STAGE 1

Au commencement, votre machine n'est pas connectée au réseau : vous allez
devoir le configurer manuellement.

Commencez par prendre connaissance de la topologie réseau.

Afin de ne pas créer de conflit d'IP sur le réseau, veuillez respecter les
plages qui vous sont attribuées. Si néanmoins des conflits se créent, vous
disposez du fabuleux **tcpdump**(1), pour vous sortir du pétrin !

Cette étape sera validée lorsque vous pourrez contacter le serveur de
validation :

**adlin stage1 | curl -d @- http://172.23.0.1/iamalive**

Voir aussi : **modprobe**(8), **fbi**(1), **ip**(8), **ip-link**(8), **ip-address**(8)


## DON'T PANIC

Vous avez vu comment passer des arguments au noyau, via la ligne de commande
dans le *bootloader*. Certains paramètres peuvent également être changé pendant
que le noyau est lancé.

Par exemple, vous pouvez consulter à partir de quel numéro de port, il n'y a plus besoin de privilèges pour l'utiliser :

**cat /proc/sys/net/ipv4/ip_unprivileged_port_start**

Et vous pouvez le modifier, aussi simplement que cela :

**echo 142 > /proc/sys/net/ipv4/ip_unprivileged_port_start**

PS : c'est un exemple, ça ne sert à rien de modifier cette valeur, par contre d'autres ...

Voir aussi : **sysctl**(8), **proc**(5) (rechercher */proc/sys*, */proc/sys/kernel*)


## STAGE 2

Une DMZ contenant différents services est à votre disposition. Commencez par
établir un lien avec le serveur de validation s'y trouvant :

**adlin stage2 | curl -d @- http://172.23.200.1/challenge**

Voir aussi : **ip-route**(8)


## BONUS 1

Souriez, vous êtes monitoré ! Mais n'y aurait-il pas un contenu caché ?

**adlin** *payload* **| curl -d @- http://172.23.200.1/echorequest**

Voir aussi : **tcpdump**(1)


## STAGE 3

On passe en HTTPS ! Facile ?

**adlin stage3 | curl -d @- https://172.23.200.1/challenge**

Voir aussi: **ntpd**(1)


## BONUS 2

Montez le disque attaché à votre machine pour y découvrir le fichier contenant
le *token* bonus.

**adlin $(cat bonus2) | curl -d @- https://172.23.200.1/testdisk**

Voir aussi: **mount**(8)


## STAGE 4

Configurez le résolveur DNS de votre système, afin d'utiliser le serveur de
noms de la DMZ. Nous cherchons un token caché dans le domaine
**adlin.nemunai.re**.

**dig +short adlin.nemunai.re** *entry-type* **| adlin stage4 | curl -d @- https://172.23.200.1/challenge**

Voir aussi: **resolv.conf**(5), **dig**(1)


## BONUS 3

Parmi les services mis à disposition dans la DMZ, se trouve un serveur de
transport de courriels (MTA). Utilisez-le pour envoyer votre meilleure histoire
drôle : des points bonus faciles !

**sendmail** [*OPTIONS*] **adlin@nemunai.re**

Voir aussi : **sendmail**(1)


## STAGE 5

Vous pouvez résoudre des noms de domaines, mais vous n'avez pas encore accès à
internet ? Consultez la topologie pour vous ajouter une route vers votre
sauvetage !

Votre passerelle vers Internet est accessible via un tunnel Wireguard. Vous
devrez d'abord configurer ce tunnel en utilisant l'outil **wg-adlin** (appelé
sans argument, il fera le nécessaire pour vous générer une configuration
adaptée).

**adlin stage5 | curl -d @- https://adlin.nemunai.re/challenge**

Voir aussi : **ip-link**(8), **wg**(8), **traceroute**(8), **tcpdump**(1), **sysctl**(8)


## STAGE 6

Générez une clef SSH robuste puis soumettez-là :

**adlin $(cat ~/.ssh/***id_ssh.pub***) | curl -d @- https://adlin.nemunai.re/sshkeys**

Voir aussi : **ssh-keygen**(1)


## STAGE 7

Pouvez-vous vous connecter ?

**ssh ***login_x***@zion.adlin.nemunai.re**


# VALEUR RENVOYÉE

Une fois le premier palier passé, utilisez le programme **progression(1)** pour
avoir un aperçu de votre avancement.

Votre niveau d'avancement dans ce premier TP permet d'établir la première note
de ce cours. Les autres notes seront établies sur vos réponses à des questions
de cours entre les sessions ainsi que sur les deux projets que l'on commencera
aux prochains TP.


# VOIR AUSSI

* <http://www.opsschool.org/> - Ops School Curriculum
* <https://github.com/phyver/GameShell> - a game to learn how to use standard commands in a Unix shell (in french)
* The Practice of System and Network Administration - T. Limoncelli, C. Hogan, S. Chalup
* Programmation système en C sous Linux - Christophe Blaess – Eyrolles


# HISTORIQUE

*2023* - Sixième édition du cours à destination des SRS 2024.

    Immersion encore plus grande dans le SI ?

*2022* - Cinquième édition du cours à destination des SRS 2023.

    Travail sur le réalisme des exercices.

*2021* - Quatrième édition du cours à destination des SRS 2022.

    Introduction de références à Matrix.

*2020* - Troisième édition du cours à destination des SRS 2021.

    Des étudiants avec involontairement de bonnes idées d'exercices supplémentaires.

*2019* - Deuxième édition du cours à destination des SRS 2020.

    Le chaos s'est un peu trop invité.

*2018* - Première édition du cours à destination des SRS 2019.

    Oops la salle machine...


# CONFORMITÉ

Certifié non conforme.
