#!/usr/bin/env python3

import base64
import email
import email.policy
import hashlib
import hmac
import json
import os
import re
import sys
import time
import urllib.request

def readmail(fp):
    theEMail = fp.read()

    msg = email.message_from_bytes(theEMail, policy=email.policy.default)
    rp = msg.get("Return-Path") or "someone"
    cnt = msg.get_content()

    return msg, rp, cnt

if __name__ == '__main__':
    # Parse command line arguments
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--url-token-validator', default="https://adlin.nemunai.re/api/recv-mail",
                        help="URL to token-validator")

    parser.add_argument('--secret', default="adelina",
                        help="Secret used in token HMAC")

    args = parser.parse_args()

    msg, rp, cnt = readmail(sys.stdin.buffer)
    hostnames = re.findall(r"<[^@]+@adlin-([^>]+)>", rp)
    if len(hostnames) == 1:
        h = hashlib.sha512()
        h.update(cnt.encode())

        req = urllib.request.Request(
            url=args.url_token_validator,
            method='POST',
            headers={
                "X-ADLIN-Authentication": base64.b64encode(hmac.digest(args.secret.encode(), str(int(time.mktime(time.localtime())/10)).encode(), hashlib.sha512)),
            },
            data=json.dumps({
                "login": hostnames[0],
                "token": h.hexdigest(),
            }).encode(),
        )
        print("validating challenge for %s" % hostnames[0])
        with urllib.request.urlopen(req) as f:
            print(f.read().decode('utf-8'))
