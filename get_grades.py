#!/usr/bin/env python3

# Usage : ./get_grades.py | jq -r 'to_entries | map([.key, .value.TP1, .value.TP2, .value.TP3]) | .[] | @csv'

# Sync to atsebay.t : ./get_grades.py | jq -r '[to_entries | .[] | {login: .key, score: .value.TP1}]' | curl -X PUT -d @- 'https://srs.nemunai.re/api/works/$ID_TP1/grades' -H 'Cookie: auth=foobar'

import base64
import hashlib
import hmac
import os
import sys
import time
import urllib.request

if __name__ == '__main__':
    # Parse command line arguments
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--url-token-validator', default="https://adlin.nemunai.re/api/",
                        help="URL to token-validator")

    parser.add_argument('--secret', default="adelina",
                        help="Secret used in token HMAC")

    args = parser.parse_args()

    req = urllib.request.Request(
        url=args.url_token_validator + "grades",
        method='GET',
        headers={
            "X-ADLIN-Authentication": base64.b64encode(hmac.digest(args.secret.encode(), str(int(time.mktime(time.localtime())/10)).encode(), hashlib.sha512)),
        },
    )
    with urllib.request.urlopen(req) as f:
        sys.stdout.write(f.read().decode('utf-8'))
