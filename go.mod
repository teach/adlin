module git.nemunai.re/srs/adlin

go 1.17

require (
	github.com/coreos/go-oidc/v3 v3.9.0
	github.com/go-sql-driver/mysql v1.7.1
	github.com/jcmturner/gokrb5/v8 v8.4.4
	github.com/julienschmidt/httprouter v1.3.0
	github.com/miekg/dns v1.1.58
	github.com/prometheus-community/pro-bing v0.3.0
	golang.org/x/oauth2 v0.17.0
)

require (
	github.com/go-jose/go-jose/v3 v3.0.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/go-uuid v1.0.3 // indirect
	github.com/jcmturner/aescts/v2 v2.0.0 // indirect
	github.com/jcmturner/dnsutils/v2 v2.0.0 // indirect
	github.com/jcmturner/gofork v1.7.6 // indirect
	github.com/jcmturner/rpc/v2 v2.0.3 // indirect
	golang.org/x/crypto v0.19.0 // indirect
	golang.org/x/mod v0.14.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/tools v0.17.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
