package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/jcmturner/gokrb5/v8/client"
	"github.com/jcmturner/gokrb5/v8/config"
	"github.com/jcmturner/gokrb5/v8/iana/etypeID"
	"github.com/jcmturner/gokrb5/v8/krberror"
	"github.com/julienschmidt/httprouter"

	"git.nemunai.re/srs/adlin/libadlin"
)

var AuthFunc = checkAuthKrb5

func init() {
	router.GET("/api/auth", apiAuthHandler(validateAuthToken))
	router.POST("/api/auth", apiRawHandler(func(w http.ResponseWriter, ps httprouter.Params, body []byte) (interface{}, error) {
		return AuthFunc(w, ps, body)
	}))
	router.POST("/api/auth/logout", apiRawHandler(logout))
}

func validateAuthToken(s *adlin.Student, _ httprouter.Params, _ []byte) (interface{}, error) {
	if s.DelegatedDomain != nil {
		for _, ddomain := range adlin.DelegatedDomainSuffixes {
			if strings.HasSuffix(*s.DelegatedDomain, ddomain) {
				s.DelegatedDomain = nil
				break
			}
		}
	}

	return s, nil
}

func logout(w http.ResponseWriter, ps httprouter.Params, body []byte) (interface{}, error) {
	http.SetCookie(w, &http.Cookie{
		Name:     "auth",
		Value:    "",
		Path:     baseURL + "/",
		Expires:  time.Unix(0, 0),
		Secure:   true,
		HttpOnly: true,
	})

	return true, nil
}

type loginForm struct {
	Username string
	Password string
}

func completeAuth(w http.ResponseWriter, username string, session *adlin.Session) (err error) {
	var std *adlin.Student
	if !adlin.StudentExists(username) {
		if std, err = adlin.NewStudent(username); err != nil {
			log.Printf("Unable to NewStudent(%s): %s", username, err)
			return fmt.Errorf("Quelque chose s'est mal passé lors de la création de ton compte (première connexion ?).")
		}
	} else if std, err = adlin.GetStudentByLogin(username); err != nil {
		log.Printf("Unable to GetStudentByLogin(%s): %s", username, err)
		return fmt.Errorf("Quelque chose s'est mal passé lors de l'accès à ton compte. Réessaye dans quelques instants.")
	}

	if session == nil {
		session, err = std.NewSession()
	} else {
		_, err = session.SetStudent(std)
	}

	if err != nil {
		log.Println("Session creation/retrieval problem:", err)
		return fmt.Errorf("Quelque chose s'est mal passé lors de l'attribution de ta session. Réessaye dans quelques instants.")
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "auth",
		Value:    base64.StdEncoding.EncodeToString(session.Id),
		Path:     baseURL + "/",
		Expires:  time.Now().Add(30 * 24 * time.Hour),
		Secure:   true,
		HttpOnly: true,
	})

	return nil
}

func dummyAuth(w http.ResponseWriter, _ httprouter.Params, body []byte) (interface{}, error) {
	var lf loginForm
	if err := json.Unmarshal(body, &lf); err != nil {
		log.Println("Bad auth request:", err)
		return nil, fmt.Errorf("Unable to unmarshal your request: %w", err)
	}

	return map[string]string{"status": "OK"}, completeAuth(w, lf.Username, nil)
}

func checkAuthHttp(w http.ResponseWriter, _ httprouter.Params, body []byte) (interface{}, error) {
	var lf loginForm
	if err := json.Unmarshal(body, &lf); err != nil {
		return nil, err
	}

	if r, err := http.NewRequest("GET", "https://owncloud.srs.epita.fr/remote.php/dav/", nil); err != nil {
		return nil, err
	} else {
		r.SetBasicAuth(lf.Username, lf.Password)

		if resp, err := http.DefaultClient.Do(r); err != nil {
			return nil, err
		} else {
			defer resp.Body.Close()

			if resp.StatusCode == http.StatusOK {
				return dummyAuth(w, nil, body)
			} else {
				return nil, errors.New(`{"status": "Invalid username or password"}`)
			}
		}
	}
}

func parseETypes(s []string, w bool) []int32 {
	var eti []int32
	for _, et := range s {
		if !w {
			var weak bool
			for _, wet := range strings.Fields(config.WeakETypeList) {
				if et == wet {
					weak = true
					break
				}
			}
			if weak {
				continue
			}
		}
		i := etypeID.EtypeSupported(et)
		if i != 0 {
			eti = append(eti, i)
		}
	}
	return eti
}

func checkAuthKrb5(w http.ResponseWriter, _ httprouter.Params, body []byte) (interface{}, error) {
	var lf loginForm
	if err := json.Unmarshal(body, &lf); err != nil {
		return nil, err
	}

	cnf := config.New()
	cnf.LibDefaults.DNSLookupKDC = true
	cnf.LibDefaults.DNSLookupRealm = true
	cnf.LibDefaults.DefaultTGSEnctypeIDs = parseETypes(cnf.LibDefaults.DefaultTGSEnctypes, cnf.LibDefaults.AllowWeakCrypto)
	cnf.LibDefaults.DefaultTktEnctypeIDs = parseETypes(cnf.LibDefaults.DefaultTktEnctypes, cnf.LibDefaults.AllowWeakCrypto)
	cnf.LibDefaults.PermittedEnctypeIDs = parseETypes(cnf.LibDefaults.PermittedEnctypes, cnf.LibDefaults.AllowWeakCrypto)

	c := client.NewWithPassword(lf.Username, "CRI.EPITA.FR", lf.Password, cnf)
	if err := c.Login(); err != nil {
		if errk, ok := err.(krberror.Krberror); ok {
			if errk.RootCause == krberror.NetworkingError {
				return nil, errors.New(`{"status": "Authentication system unavailable, please retry."}`)
			} else if errk.RootCause == krberror.KDCError {
				return nil, errors.New(`{"status": "Invalid username or password"}`)
			}
		}
		return nil, err
	} else {
		return dummyAuth(w, nil, body)
	}
}
