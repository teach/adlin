#!/bin/sh

/root/wg-sync.sh

# Clean wg
while true
do
    sleep 900
    wg show wg-adlin dump | egrep "^\S+\s+\S+\s+\S+\s+\(none\)" | while read pubkey end; do wg set wg-adlin peer $pubkey remove; done
done
