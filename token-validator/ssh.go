package main

import (
	"crypto/hmac"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"

	"github.com/julienschmidt/httprouter"

	"git.nemunai.re/srs/adlin/libadlin"
)

var AuthorizedKeysLocation = "/root/.ssh/authorized_keys"
var SshPiperLocation = "/var/sshpiper/"

func init() {
	router.GET("/sshkeys", apiHandler(
		func(httprouter.Params, []byte) (interface{}, error) {
			return adlin.GetStudentKeys()
		}))
	router.POST("/sshkeys", rawHandler(responseHandler(receiveKey)))
	router.GET("/sshkeys/authorizedkeys", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		dumpAuthorizedKeysFile(w)
	})
	router.GET("/api/students/:sid/hassshkeys", apiHandler(studentHandler(hasSSHKeys)))
	router.GET("/api/students/:sid/authorizedkeys", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if sid, err := strconv.Atoi(string(ps.ByName("sid"))); err != nil {
			if student, err := adlin.GetStudentByLogin(ps.ByName("sid")); err != nil {
				http.Error(w, "Student doesn't exist.", http.StatusNotFound)
			} else {
				dumpStdAuthorizedKeysFile(student, w)
			}
		} else if student, err := adlin.GetStudent(sid); err != nil {
			http.Error(w, "Student doesn't exist.", http.StatusNotFound)
		} else {
			dumpStdAuthorizedKeysFile(student, w)
		}

	})
}

func hasSSHKeys(student *adlin.Student, body []byte) (interface{}, error) {
	if keys, err := student.GetKeys(); err != nil {
		return nil, err
	} else {
		return len(keys) > 0, nil
	}
}

func receiveKey(r *http.Request, ps httprouter.Params, body []byte) (interface{}, error) {
	var gt givenToken
	if err := json.Unmarshal(body, &gt); err != nil {
		return nil, err
	}

	gt.token = make([]byte, hex.DecodedLen(len(gt.Token)))
	if _, err := hex.Decode(gt.token, []byte(gt.Token)); err != nil {
		return nil, err
	}

	if std, err := adlin.GetStudentByLogin(gt.Login); err != nil {
		return nil, err
	} else if len(gt.Data) < 2 {
		return nil, errors.New("No key found!")
	} else {
		pkey := std.GetPKey()

		data := [][]byte{}
		for _, d := range gt.Data {
			data = append(data, []byte(d))
		}

		if expectedToken, err := GenerateToken(pkey, 0, data...); err != nil {
			return nil, err
		} else if !hmac.Equal(expectedToken, gt.token) {
			return nil, errors.New("This is not the expected token.")
		}

		if _, err := std.NewKey(gt.Data[0] + " " + gt.Data[1]); err != nil {
			return nil, err
		}

		log.Printf("%s just pushed sshkey\n", std.Login)

		if len(AuthorizedKeysLocation) > 0 {
			file, err := os.Create(AuthorizedKeysLocation)
			if err != nil {
				log.Fatal("Cannot create file", err)
				goto sshpiperimport
			}
			defer file.Close()

			dumpAuthorizedKeysFile(file)
		}

	sshpiperimport:
		if len(SshPiperLocation) > 0 {
			if err := os.MkdirAll(path.Join(SshPiperLocation, std.Login), 0777); err != nil {
				log.Fatal("Cannot create sshpiper directory:", err)
			} else {
				file, err := os.Create(path.Join(SshPiperLocation, std.Login, "authorized_keys"))
				if err != nil {
					log.Fatal("Cannot create sshpiperd file", err)
					goto onerr
				}
				defer file.Close()

				dumpStdAuthorizedKeysFile(std, file)
				os.Symlink(path.Join(SshPiperLocation, "sshpiper_upstream"), path.Join(SshPiperLocation, std.Login, "sshpiper_upstream"))
				os.Symlink(path.Join(SshPiperLocation, "id_rsa"), path.Join(SshPiperLocation, std.Login, "id_rsa"))
			}
		}

	onerr:
		return "Key imported", nil
	}
}

func dumpAuthorizedKeysFile(w io.Writer) {
	seen := map[string]interface{}{}

	if keys, _ := adlin.GetStudentKeys(); keys != nil {
		for _, k := range keys {
			if _, exists := seen[k.Key]; exists {
				continue
			} else {
				seen[k.Key] = true
			}

			s, _ := k.GetStudent()
			w.Write([]byte("command=\"/root/adlin.sh " + fmt.Sprintf("%d", k.IdStudent) + " '" + s.Login + "'\",restrict " + k.Key + fmt.Sprintf(" Student#%d-%q\n", k.IdStudent, s.Login)))
		}
	}
}

func dumpStdAuthorizedKeysFile(s *adlin.Student, w io.Writer) {
	seen := map[string]interface{}{}

	if keys, _ := s.GetKeys(); keys != nil {
		for _, k := range keys {
			if _, exists := seen[k.Key]; exists {
				continue
			} else {
				seen[k.Key] = true
			}

			s, _ := k.GetStudent()
			w.Write([]byte(k.Key + fmt.Sprintf(" Student#%d-%q\n", k.IdStudent, s.Login)))
		}
	}
}
