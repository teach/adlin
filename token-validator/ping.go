package main

import (
	"encoding/json"
	"errors"

	"git.nemunai.re/srs/adlin/libadlin"
)

var PongSecret = "felixfixit"

func init() {
	router.GET("/api/students/:sid/ping", apiHandler(studentHandler(lastPing)))
	router.GET("/api/students/:sid/pong", apiHandler(studentHandler(func(student *adlin.Student, body []byte) (interface{}, error) {
		return student.LastPongs()
	})))
	router.POST("/api/students/:sid/pong", apiHandler(studentHandler(stdPong), sslOnly))
}

func lastPing(student *adlin.Student, body []byte) (interface{}, error) {
	if pongs, err := student.LastPongs(); err != nil {
		return nil, err
	} else if len(pongs) <= 0 {
		return false, nil
	} else {
		return pongs[0], nil
	}
}

func stdPong(student *adlin.Student, body []byte) (interface{}, error) {
	var gt givenToken
	if err := json.Unmarshal(body, &gt); err != nil {
		return nil, err
	}

	if gt.Token != PongSecret {
		return nil, errors.New("This is not the expected token.")
	}

	return true, student.OnPong(gt.Challenge == 0)
}
