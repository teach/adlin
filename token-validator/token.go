package main

import (
	"crypto/sha512"
	"fmt"
)

func GenerateToken(pkey []byte, id int, a ...[]byte) ([]byte, error) {
	h := sha512.New()

	h.Write([]byte(fmt.Sprintf("%x", pkey)))
	// Challenge toctoc(6) should not have that
	if id != 0 && id != 6 {
		h.Write([]byte(fmt.Sprintf(":%d", id)))
	}

	for _, v := range a {
		h.Write([]byte(":"))
		h.Write(v)
	}

	return h.Sum(nil), nil
}
