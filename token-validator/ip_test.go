package main

import (
	"log"
	"net"
	"testing"

	"git.nemunai.re/srs/adlin/libadlin"
)

func TestIPSuffix(t *testing.T) {
	network := net.IPNet{IP: net.ParseIP("172.23.0.0"), Mask: net.CIDRMask(17, 32)}

	std := &adlin.Student{Id: 1}
	ip := IPSuffix(std, network)

	log.Println(ip)

	if ip.String() != "172.23.0.11" {
		t.Fatalf(`IPSuffix(%v, %q) = %q, expected %q`, std, network.String(), ip.String(), "172.23.0.11")
	}

	std = &adlin.Student{Id: 60}
	ip = IPSuffix(std, network)

	if ip.String() != "172.23.0.247" {
		t.Fatalf(`IPSuffix(%v, %q) = %q, expected %q`, std, network.String(), ip.String(), "172.23.0.247")
	}

	std = &adlin.Student{Id: 62}
	ip = IPSuffix(std, network)

	if ip.String() != "172.23.1.1" {
		t.Fatalf(`IPSuffix(%v, %q) = %q, expected %q`, std, network.String(), ip.String(), "172.23.1.1")
	}

	std = &adlin.Student{Id: 70}
	ip = IPSuffix(std, network)

	if ip.String() != "172.23.1.33" {
		t.Fatalf(`IPSuffix(%v, %q) = %q, expected %q`, std, network.String(), ip.String(), "172.23.1.33")
	}

	// Other network
	network = net.IPNet{IP: net.ParseIP("192.168.42.0"), Mask: net.CIDRMask(17, 32)}

	std = &adlin.Student{Id: 70}
	ip = IPSuffix(std, network)

	if ip.String() != "192.168.43.33" {
		t.Fatalf(`IPSuffix(%v, %q) = %q, expected %q`, std, network.String(), ip.String(), "192.168.43.33")
	}
}
