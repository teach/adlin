package main

import (
	"embed"
	"flag"
	"io/fs"
	"log"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
)

var (
	//go:embed htdocs
	assets   embed.FS
	staticFS staticDevDir
)

type staticDevDir struct {
	fs   *http.FileSystem
	Path *string
}

func (i *staticDevDir) String() string {
	if i.Path == nil {
		return "embeded"
	}
	return *i.Path
}

func (i *staticDevDir) Set(value string) error {
	i.Path = &value
	return nil
}

func (i *staticDevDir) GetFS() *http.FileSystem {
	if i.fs == nil {
		if i.Path != nil {
			v := http.FileSystem(http.Dir(*i.Path))
			i.fs = &v
		} else if sub, err := fs.Sub(assets, "htdocs"); err != nil {
			log.Fatal("Unable to cd to htdocs/ directory in embeded FS:", err)
		} else {
			v := http.FS(sub)
			i.fs = &v
		}
	}
	return i.fs
}

func serveFile(w http.ResponseWriter, r *http.Request, url string) {
	r.URL.Path = url
	http.FileServer(*staticFS.GetFS()).ServeHTTP(w, r)
}

func init() {
	flag.Var(&staticFS, "static", "Directory containing static files (default if not provided: use embedded files)")

	Router().GET("/", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, "/maatma.html")
	})
	Router().GET("/auth", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, "/maatma.html")
	})
	Router().GET("/domains", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, "/maatma.html")
	})
	Router().GET("/tunnels", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, "/maatma.html")
	})
	Router().GET("/css/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, r.URL.Path)
	})
	Router().GET("/fonts/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, r.URL.Path)
	})
	Router().GET("/img/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, r.URL.Path)
	})
	Router().GET("/js/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, r.URL.Path)
	})
	Router().GET("/views/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, r.URL.Path)
	})

	Router().GET("/dashboard/", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, "/dashboard.html")
	})
	Router().GET("/dashboard/:where", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		serveFile(w, r, "/dashboard.html")
	})
	Router().GET("/dashboard/:where/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if ps.ByName("where") == "css" {
			w.Header().Set("Content-Type", "text/css")
			serveFile(w, r, strings.TrimPrefix(r.URL.Path, "/dashboard"))
		} else if ps.ByName("where") == "js" {
			w.Header().Set("Content-Type", "text/javascript")
			serveFile(w, r, strings.TrimPrefix(r.URL.Path, "/dashboard"))
		} else {
			http.NotFound(w, r)
		}
	})
}

func sanitizeStaticOptions() error {
	return nil
}
