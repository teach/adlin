angular.module("AdLinApp", ["ngRoute", "ngResource", "ngSanitize"])
    .config(function($routeProvider, $locationProvider) {
	$routeProvider
	    .when("/auth", {
		controller: "AuthController",
		templateUrl: "views/auth.html"
	    })
	    .when("/domains", {
		controller: "DomainsController",
		templateUrl: "views/domains.html"
	    })
	    .when("/tunnels", {
		controller: "TunnelsController",
		templateUrl: "views/tunnels.html"
	    })
	    .when("/", {
		templateUrl: "views/home.html"
	    });
	$locationProvider.html5Mode(true);
    });

angular.module("AdLinApp")
    .factory("Student", function($resource) {
	return $resource("/api/students/:studentId", { studentId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("Progression", function($resource) {
	return $resource("/api/progress")
    })
    .factory("Challenge", function($resource) {
	return $resource("/challenge/:challengeId", { challengeId: '@id' })
    });

angular.module("AdLinApp")
    .filter("since", function() {
      return function(passed) {
        passed -= Date.now()/1000
	if (passed < 120) {
	  return Math.floor(-passed) + " seconds ago";
	} else {
	  return Math.floor(-passed/60) + " minutes ago";
	}
      }
    })

    .directive('integer', function() {
	return {
	    require: 'ngModel',
	    link: function(scope, ele, attr, ctrl){
		ctrl.$parsers.unshift(function(viewValue){
		    return parseInt(viewValue, 10);
		});
	    }
	};
    })

    .component('toast', {
      bindings: {
        date: '=',
        msg: '=',
        title: '=',
        variant: '=',
      },
      controller: function($element) {
        $element.children(0).toast('show')
      },
      template: `<div class="toast mb-2" role="alert" aria-live="assertive" aria-atomic="true" data-delay="7000">
  	<div class="toast-header">
            <span ng-if="$ctrl.variant" class="badge badge-pill badge-{{ $ctrl.variant }}" style="padding: .25em .66em">&nbsp;</span>&nbsp;
  	  <strong class="mr-auto" ng-bind="$ctrl.title"></strong>
  	  <small class="text-muted" ng-bind="$ctrl.date">just now</small>
  	  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
  	    <span aria-hidden="true">&times;</span>
  	  </button>
  	</div>
  	<div class="toast-body" ng-bind-html="$ctrl.msg"></div>
        </div>`
    })

    .run(function($rootScope, $interval, $http) {
        $rootScope.toasts = [];

        $rootScope.addToast = function(toast) {
	  $rootScope.toasts.unshift(toast);
        }

	$rootScope.checkLoginState = function() {
	    $http({
		method: 'GET',
		url: "api/auth",
	    }).then(function(response) {
		$rootScope.isLogged = response.data;
		$rootScope.student = response.data;
	    }, function(response) {
		$rootScope.isLogged = false;
	    });
	};
	$rootScope.checkLoginState();
	$interval($rootScope.checkLoginState, 40000);

	$rootScope.disconnectCurrentUser = function() {
	    $http({
		method: 'POST',
		url: "api/auth/logout"
	    }).then(function(response) {
		$rootScope.isLogged = false;
		$rootScope.student = null;
	    });
	}
    })

    .controller("ProgressionController", function($scope, $interval, $http) {
        $scope.tuto_progress = tuto_progress;
        $scope.tuto_legend = tuto_legend;

	$scope.mychallenges = {};
	var refreshChal = function() {
	    $http.get("api/students/" + $scope.student.id + "/progress").then(function(response) {
		$scope.mychallenges = response.data
	    });
	}
	refreshChal();
	var myinterval = $interval(refreshChal, 15750);
        $scope.$on('$destroy', function () { $interval.cancel(myinterval); });
    })

    .controller("AuthController", function($scope, $rootScope, $http, $location) {
	$scope.auth = {
	    "username": "",
	    "password": "",
	};

	$scope.logmein = function() {
	    $scope.pleaseWait = true;
	    $http({
		method: 'POST',
		url: "api/auth",
		data: $scope.auth
	    }).then(function(response) {
		$scope.pleaseWait = false;
		$rootScope.checkLoginState();
		$location.url("/");
	    }, function(response) {
		$scope.pleaseWait = false;
		if (response.data)
	          $scope.addToast({
	            variant: "danger",
	            title: "Connexion impossible",
	            msg: (response.data ? response.data.errmsg : "Impossible de contacter le serveur"),
	          });
	    });
	}
    })

    .controller("TunnelsController", function($scope, $http, $interval) {
	$scope.updateTunnelInfo = function() {
	    $http({
		method: 'GET',
		url: "api/wginfo",
	    }).then(function(response) {
		$scope.wginfo = response.data;
	    });
	};
	$scope.updateTunnelInfo();

        var noUpdate = 0

        $scope.updateTunnelsList = function() {
          if (noUpdate == 0)
	    $http({
		method: 'GET',
		url: "api/wg/",
	    }).then(function(response) {
		$scope.tunnels = response.data;
	    }, function(response) {
	      $scope.addToast({
	        variant: "warning",
	        title: "Maatma Tunnels",
	        msg: "Nous rencontrons actuellement des difficultés avec la gestion des tunnels : " + (response.data ? response.data.errmsg : "Impossible de contacter le serveur") + "<br>Veuillez réessayer dans quelques instants.",
	      });
	    });
	};
	$scope.updateTunnelsList();
        var myinterval = $interval($scope.updateTunnelsList, 12000);
        $scope.$on('$destroy', function () { $interval.cancel(myinterval); });

        $scope.PING_ok = false;
        $scope.PING_time = '';
        $scope.PING = false;
	var refreshPing = function() {
	    $http.get("/api/students/" + $scope.student.id + "/ping").then(function(response) {
		$scope.PING_ok = response.data.State;
		$scope.PING_time = new Date(response.data.Date);
		$scope.PING = (Date.now() - $scope.PING_time)/1000;
	    });
	}
        var myPingInterval
        $scope.$watch("student", function(student) {
          if (student == null) return;
          $interval.cancel(myPingInterval);
	  refreshPing();
          myPingInterval = $interval(refreshPing, 15000);
        })
        $scope.$on('$destroy', function () { $interval.cancel(myPingInterval); });

	$scope.newTunnel = function() {
	    $scope.pleaseWaitNew = true;
	    $http({
		method: 'POST',
		url: "api/wg/",
		data: {}
	    }).then(function(response) {
		$scope.updateTunnelsList();
		$scope.pleaseWaitNew = false;
	        $scope.addToast({
	          variant: "success",
	          title: "Maatma Tunnels",
	          msg: "Tunnel créé avec succès !",
	        });
	    }, function(response) {
		$scope.pleaseWaitNew = false;
	        $scope.addToast({
	          variant: "danger",
	          title: "Maatma Tunnels",
	          msg: (response.data ? response.data.errmsg : "Impossible de contacter le serveur"),
	        });
	    });
	}

        $scope.editTunnel = function(tunnel) {
          tunnel.edit = true;
          noUpdate++;
          tunnel.newData = {
            TokenText: tunnel.TokenText,
            SuffixIP: tunnel.SuffixIP,
          }
        };

        $scope.updateTunnel = function(tunnel) {
	    tunnel.pleaseWaitUpdate = true;
	    $http({
		method: 'PUT',
	        url: "api/wg/" + encodeURIComponent(tunnel.TokenText),
	        data: tunnel.newData
	    }).then(function(response) {
                noUpdate--;
                tunnel.SuffixIP = tunnel.newData.SuffixIP;
                tunnel.TokenText = tunnel.newData.TokenText;
                tunnel.edit = false;
	        tunnel.pleaseWaitUpdate = false;
		$scope.updateTunnelsList();
	        $scope.addToast({
	          variant: "success",
	          title: "Maatma Tunnels",
	          msg: "Tunnel mise à jour avec succès !",
	        });
	    }, function(response) {
		tunnel.pleaseWaitUpdate = false;
	        $scope.addToast({
	          variant: "danger",
	          title: "Maatma Tunnels",
	          msg: (response.data ? response.data.errmsg : "Impossible de contacter le serveur"),
	        });
	    });
        }

	$scope.dropTunnel = function(tunnel) {
	    tunnel.pleaseWaitDrop = true;
	    $http({
		method: 'DELETE',
	        url: "api/wg/" + encodeURIComponent(tunnel.TokenText),
		data: {}
	    }).then(function(response) {
		$scope.updateTunnelsList();
		tunnel.pleaseWaitDrop = false;
	        $scope.addToast({
	          variant: "success",
	          title: "Maatma Tunnels",
	          msg: "Tunnel supprimé avec succès !",
	        });
	    }, function(response) {
		tunnel.pleaseWaitDrop = false;
	        $scope.addToast({
	          variant: "danger",
	          title: "Maatma Tunnels",
	          msg: (response.data ? response.data.errmsg : "Impossible de contacter le serveur"),
	        });
	    });
	}
    })

    .controller("DomainsController", function($scope, $http, $location) {
	var onUpdateEvt = [];
	var callOnUpdateEvt = function() {
	    angular.forEach(onUpdateEvt, function(cb) {
		cb();
	    });
	}

	$scope.addOnUpdateEvent = function(cb) {
	    onUpdateEvt.push(cb);
	}

	$scope.updateAssociationD = function() {
	    $http({
		method: 'GET',
		url: "api/adomains/",
	    }).then(function(response) {
		$scope.adomains = [];
		response.data.forEach(function(domain) {
		    $http({
			method: 'GET',
			url: "api/adomains/" + domain,
		    }).then(function(response) {
                      if (response.data) {
			response.data.forEach(function(rr) {
			  $scope.adomains.push(rr);
			});
                      }
		    });
		}, function(response) {
		    console.log(response.data.errmsg);
		});
	    });
	};
	$scope.updateAssociationD();

	$scope.updateDelegatedD = function() {
	    $http({
		method: 'GET',
		url: "api/ddomains/",
	    }).then(function(response) {
		response.data.forEach(function(domain) {
		    $scope.ddomains = response.data;
		}, function(response) {
		    alert(response.data.errmsg);
		});
	    });
	};
        $scope.updateDelegatedD();

        $scope.askAssociationD = function() {
            if ($scope.adomains.length == 0) {
                return $scope.newAssociationD({})
            }

            var aaaa = ""
            if ($scope.adomains.length >= 1) {
              $scope.adomains.forEach(function (adomain) {
                if (adomain.values.join("").slice(0, 5) == "2a01:")
                  aaaa = adomain.values.join("")
              })
            }

	    $scope.assoc = {
		"domain": $scope.adomains[0].domain,
		"a": "82.64.151.41",
		"aaaa": aaaa,
	    }
	    $('#AssocModal').modal('show');
	}

	$scope.newAssociationD = function(assoc) {
	    $('#AssocModal').modal('hide');
	    $scope.pleaseWaitNewAssociation = true;
	    $http({
		method: 'POST',
	        url: "api/adomains/",
                data: assoc,
	    }).then(function(response) {
		$scope.updateAssociationD();
		$scope.pleaseWaitNewAssociation = false;
	        $scope.addToast({
	          variant: "success",
	          title: "Maatma Domain Names",
	          msg: "Nouvelle association créée avec succès !",
	        });
	    }, function(response) {
		$scope.pleaseWaitNewAssociation = false;
	        $scope.addToast({
	          variant: "danger",
	          title: "Maatma Domain Names",
	          msg: "Erreur durant l'attribution du domaine : " + response.data.errmsg,
	        });
	    });
	}

        $scope.useMyAssociationD = function() {
	    $scope.assoc = {
		"domain": $scope.adomains[0].domain,
	        "cname": $scope.student.associated_domain?$scope.student.associated_domain:"",
	    }
	    $('#AssocMyDomainModal').modal('show');
	}

	$scope.newMyDomainAssociationD = function(assoc) {
	    $('#AssocMyDomainModal').modal('hide');
	    $scope.pleaseWaitNewAssociation = true;
	    $http({
		method: 'POST',
	        url: "api/adomains/",
                data: assoc,
	    }).then(function(response) {
		$scope.updateAssociationD();
                $scope.checkLoginState();
		$scope.pleaseWaitNewAssociation = false;
	        $scope.addToast({
	          variant: "success",
	          title: "Maatma Domain Names",
	          msg: "Votre domaine a bien été associé !",
	        });
	    }, function(response) {
		$scope.pleaseWaitNewAssociation = false;
	        $scope.addToast({
	          variant: "danger",
	          title: "Maatma Domain Names",
	          msg: "Erreur durant l'association du domaine : " + response.data.errmsg,
	        });
	    });
	}

	$scope.delMyDomainAssociationD = function(assoc) {
	    $('#AssocMyDomainModal').modal('hide');
	    $scope.pleaseWaitNewAssociation = true;
            assoc.cname = ''
	    $http({
		method: 'POST',
	        url: "api/adomains/",
                data: assoc,
	    }).then(function(response) {
	        $scope.updateAssociationD();
                $scope.checkLoginState();
		$scope.pleaseWaitNewAssociation = false;
	        $scope.addToast({
	          variant: "success",
	          title: "Maatma Domain Names",
	          msg: "Votre domaine n'est plus pris en compte. Vous devez utiliser l'association qui vous a été attribuée sous adlin20xx.p0m.fr.",
	        });
	    }, function(response) {
		$scope.pleaseWaitNewAssociation = false;
	        $scope.addToast({
	          variant: "danger",
	          title: "Maatma Domain Names",
	          msg: "Erreur durant l'association du domaine : " + response.data.errmsg,
	        });
	    });
	}

        $scope.useMyDelegationD = function() {
	    $scope.assoc = {
	        "ns": $scope.student.delegated_domain?$scope.student.delegated_domain:"",
	    }
	    $('#DelegateMyDomainModal').modal('show');
	}

	$scope.newMyDomainDelegationD = function(assoc) {
	    $('#DelegateMyDomainModal').modal('hide');
	    $scope.pleaseWaitNewDelegation = true;
	    $http({
		method: 'POST',
	        url: "api/ddomains/",
                data: assoc,
	    }).then(function(response) {
                $scope.checkLoginState();
		$scope.pleaseWaitNewDelegation = false;
	        $scope.addToast({
	          variant: "success",
	          title: "Maatma Domain Names",
	          msg: "Votre sous-domaine de délégation a bien été enregistré !",
	        });
	    }, function(response) {
		$scope.pleaseWaitNewDelegation = false;
	        $scope.addToast({
	          variant: "danger",
	          title: "Maatma Domain Names",
	          msg: "Erreur durant la délégation du domaine : " + response.data.errmsg,
	        });
	    });
	}

	$scope.delMyDomainDelegatedD = function() {
	    $scope.pleaseWaitNewDelegation = true;
	    $http({
		method: 'POST',
	        url: "api/ddomains/",
                data: {},
	    }).then(function(response) {
                $scope.checkLoginState();
		$scope.pleaseWaitNewDelegation = false;
	        $scope.addToast({
	          variant: "success",
	          title: "Maatma Domain Names",
	          msg: "Votre domaine n'est plus pris en compte. Vous devez utiliser la délégation qui vous a été attribuée sous srs.p0m.fr.",
	        });
	    }, function(response) {
		$scope.pleaseWaitNewDelegation = false;
	        $scope.addToast({
	          variant: "danger",
	          title: "Maatma Domain Names",
	          msg: "Erreur durant la délégation du domaine : " + response.data.errmsg,
	        });
	    });
	}

	$scope.addNS = function(domain) {
	    $scope.nsrr = {
		"domain": domain,
		"ttl": 900,
		"rr": "NS",
		"values": [""],
	    }
	    $('#NSModal').modal('show');
	}
	$scope.updateNS = function(domain, rr) {
	    $scope.nsrr = {
		"domain": domain,
		"ttl": rr.ttl,
		"rr": "NS",
	        "valuesfrom": rr.values.join(" "),
		"values": rr.values,
	    }
	    $('#NSModal').modal('show');
	}
	$scope.addGLUE = function(domain) {
	    $scope.nsrr = {
		"domain": domain,
		"ttl": 900,
		"rr": "AAAA",
		"values": [""],
	    }
	    $('#NSModal').modal('show');
	}
	$scope.updateGLUE = function(domain, rr) {
	    $scope.nsrr = {
		"domain": domain,
		"ttl": rr.ttl,
		"rr": "AAAA",
	        "valuesfrom": rr.values.join(" "),
		"values": rr.values,
	    }
	    $('#NSModal').modal('show');
	}
	$scope.addDS = function(domain) {
	    $scope.nsrr = {
		"domain": domain,
		"ttl": 900,
		"rr": "DNSKEY",
		"labels": ["Flags", "Protocole", "Algorithme", "Clef publique (base64)"],
		"values": ["", "", "", ""],
	    }
	    $('#NSModal').modal('show');
	}
	$scope.updateDS = function(domain, rr) {
	    $scope.nsrr = {
		"domain": domain,
		"ttl": rr.ttl,
		"rr": "DS",
		"labels": ["Key Tag", "Algo clef", "Type de hash", "Hash (hex)"],
	        "valuesfrom": rr.values.join(" "),
		"values": rr.values,
	    }
	    $('#NSModal').modal('show');
	}

	$scope.saveNSRR = function(nsrr) {
            if (nsrr.ttl)
                nsrr.ttl = parseInt(nsrr.ttl)
            if (nsrr.rr == "DNSKEY")
                nsrr.rr = "DS"
	    $http({
		method: (nsrr.valuesfrom !== undefined)?'PATCH':'POST',
		url: "api/ddomains/" + nsrr.domain + "/" + nsrr.rr,
		data: nsrr,
	    }).then(function(response) {
		$('#NSModal').modal('hide');
		callOnUpdateEvt();
	    }, function(response) {
		alert(response.data.errmsg);
	    });
	}

    	$scope.deleteRR = function(domain, rr) {
	    rr["pleaseWait" + rr.rr + "del"] = true;
	    $http({
		method: 'DELETE',
		url: "api/ddomains/" + domain + "/" + rr.rr,
		data: rr,
	    }).then(function(response) {
		callOnUpdateEvt();
		rr["pleaseWait" + rr.rr + "del"] = false;
	    }, function(response) {
		rr["pleaseWait" + rr.rr + "del"] = false;
		alert(response.data.errmsg);
	    });
	}
    })

    .controller("NSDomainsController", function($scope, $http) {
	var updateNS = function() {
	    $http({
		method: 'GET',
		url: "api/ddomains/" + $scope.domain + "/NS",
	    }).then(function(response) {
		$scope.domainNS = response.data;
	    });
	}
	updateNS();
	$scope.addOnUpdateEvent(updateNS);
    })

    .controller("GLUEDomainsController", function($scope, $http) {
	var updateGLUE = function() {
	    $http({
		method: 'GET',
		url: "api/ddomains/" + $scope.domain + "/GLUE",
	    }).then(function(response) {
		$scope.domainGLUE = response.data;
	    });
	}
	updateGLUE();
	$scope.addOnUpdateEvent(updateGLUE);
    })

    .controller("GLUEController", function($scope, $http) {
	var updateGLUE = function() {
	    $scope.GLUEpending = true;
	    $http({
	      method: 'POST',
	      url: "api/check/GLUE",
	      data: {domain: $scope.rr.domain, ip: $scope.rr.values.join("")}
	    }).then(function(response) {
	      $scope.GLUEpending = false;
	      $scope.GLUEok = response.data;
              $scope.GLUEerr = "OK";
	    }, function(response) {
	      $scope.GLUEpending = false;
              $scope.GLUEok = false;
              $scope.GLUEerr = response.data.errmsg;
            });
	}
	updateGLUE();
	$scope.addOnUpdateEvent(updateGLUE);
    })

    .controller("DSDomainsController", function($scope, $http) {
	$scope.dn_has_dnssec = dn_has_dnssec;
      $scope.dn_without_dnssec = dn_without_dnssec;
      console.log(dn_without_dnssec)
	var updateDS = function() {
	    $http({
		method: 'GET',
		url: "api/ddomains/" + $scope.domain + "/DS",
	    }).then(function(response) {
		$scope.domainDS = response.data;
	    });
	}
	updateDS();
	$scope.addOnUpdateEvent(updateDS);
    })
