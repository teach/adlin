angular.module("AdLinApp", ["ngResource", "ngSanitize"])
    .factory("Student", function($resource) {
	return $resource("/api/students/:studentId", { studentId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("Progression", function($resource) {
	return $resource("/api/progress")
    })
    .factory("StudentProgression", function($resource) {
        return $resource("/api/students/:studentId/progress", { studentId: '@id' })
    })
    .factory("Challenge", function($resource) {
	return $resource("/challenge/:challengeId", { challengeId: '@id' })
    });

angular.module("AdLinApp")
    .run(function($rootScope, $location) {
      if (window.location.pathname.split("/").length >= 3) {
	$rootScope.tutoid = parseInt(window.location.pathname.split("/")[2], 10);
        if (isNaN($rootScope.tutoid)) {
	  $rootScope.onestudent = window.location.pathname.split("/")[2];
        }
      }
      if (!$rootScope.tutoid || isNaN($rootScope.tutoid)) {
	$rootScope.tutoid = 1;
      }
      $rootScope.tutoid--;
      $rootScope.show_dropdown = false;
      $rootScope.toogleDropdown = function() {
        $rootScope.show_dropdown = !$rootScope.show_dropdown;
      }
    })
    .controller("StudentsController", function($scope, $interval, Student) {
	$scope.students = Student.query();
	var refreshStd = function() {
	    $scope.students = Student.query();
	}
        var myinterval = $interval(refreshStd, 1600000);
        $scope.$on('$destroy', function () { $interval.cancel(myinterval); });
    })
    .controller("StudentsProgressionController", function($scope, $interval, Progression) {
	$scope.tuto_progress = tuto_progress;
	$scope.tuto_legend = tuto_legend;
	$scope.stats = {};
        $scope.students = {};
        $scope.filterBadgeState = {};

        $scope.toogleBadge = function(ch) {
          if ($scope.filterBadgeState["challenge"] && $scope.filterBadgeState["challenge"] == ch) {
            switch ($scope.filterBadgeState["state"]) {
            case "bad":
              $scope.filterBadgeState["state"] = "passed";
              break;
            case "passed":
              $scope.filterBadgeState["state"] = "online";
              break;
            case "online":
              $scope.filterBadgeState["state"] = "offline";
              break;
            default:
              $scope.filterBadgeState = {};
            }
          } else {
            $scope.filterBadgeState["challenge"] = ch;
            $scope.filterBadgeState["state"] = "bad";
          }
        };

        var refreshStd = function() {
	  var students = Progression.get();

	  students.$promise.then(function(response) {
	    var recent = new Date(Date.now() - 120000);
	    var tmpstats = {total:0};

	    angular.forEach(tuto_progress, function(tuto) {
	      angular.forEach(tuto, function(ch, chid) {
		tmpstats[chid] = {"success":0, "warning":0};
	      });
	    });

	    angular.forEach(response, function(challenges, login) {
	      if (login[0] == "$") return;
	      tmpstats.total++;
	      angular.forEach(challenges, function(ch, chid) {
		if (ch.time && challenges[chid] !== undefined) {
		  challenges[chid].time = new Date(ch.time);
		  challenges[chid].recent = (Date.now() - ch.time)/1000;
		  if (recent < challenges[chid].time && tmpstats[chid] !== undefined)
		    tmpstats[chid].success++;
		}
		if (tmpstats[chid] !== undefined)
		  tmpstats[chid].warning++;
	      });
	      challenges["img"] = login;
	      $scope.students[login] = challenges;
	    });
	    $scope.stats = tmpstats;

	  })
	}
        refreshStd();
        var myinterval = $interval(refreshStd, 9750);
        $scope.$on('$destroy', function () { $interval.cancel(myinterval); });
    })
    .controller("StudentProgressionController", function($scope, $interval, $http, Student, StudentProgression) {
      $scope.tuto_progress = tuto_progress;
      $scope.tuto_legend = tuto_legend;
      var refreshStd = function() {
        var student = Student.get({studentId: $scope.onestudent})
        student.$promise.then(function(stdnt) {
          $scope.student = stdnt
	  $http.get("/api/students/" + $scope.student.id + "/ips").then(function(response) {
	    $scope.ips = response.data;
	  });
        })

        var mychallenges = StudentProgression.get({studentId: $scope.onestudent})
        mychallenges.$promise.then(function(mychals) {
          angular.forEach(mychals, function(ch, chid) {
	    if (ch.time) {
	      mychals[chid].time = new Date(ch.time);
	      mychals[chid].recent = (Date.now() - mychals[chid].time)/1000;
	    }
	  });
          $scope.mychallenges = mychals
        })

        $scope.img = $scope.onestudent;
      }
      $scope.$watch("onestudent", function(onestudent) {
	refreshStd();
	var myinterval = $interval(refreshStd, 15000);
        $scope.$on('$destroy', function () { $interval.cancel(myinterval); });
      })
    })
    .controller("PingController", function($scope, $interval, $http) {
	$scope.PING = false;
	$scope.PING_time = '';
        $scope.PING_ok = false;
        var refreshPing = function() {
	    $http.get("/api/students/" + $scope.student.id + "/ping").then(function(response) {
		$scope.PING_ok = response.data.State;
		$scope.PING_time = new Date(response.data.Date);
		$scope.PING = (Date.now() - $scope.PING_time)/1000;
	    });
	}
        $scope.$watch("student", function(student) {
          student.$promise.then(function(std) {
	    refreshPing();
	    var myinterval = $interval(refreshPing, 15000);
            $scope.$on('$destroy', function () { $interval.cancel(myinterval); });
          })
        })
    })
    .controller("SSHController", function($scope, $interval, $http) {
	$scope.SSH = false;
	var refreshSSH = function() {
	    $http.get("/api/students/" + $scope.student.id + "/hassshkeys").then(function(response) {
		$scope.SSH = response.data
	    });
	}
	refreshSSH();
	var myinterval = $interval(refreshSSH, 15500);
        $scope.$on('$destroy', function () { $interval.cancel(myinterval); });
    })
    .controller("ProgressionController", function($scope, $interval, $http) {
	$scope.tuto_progress = tuto_progress;
	$scope.tuto_legend = tuto_legend;
	$scope.mychallenges = {};
	var refreshChal = function() {
	    $http.get("/api/students/" + $scope.student.id + "/progress").then(function(response) {
		angular.forEach(response.data, function(ch, chid) {
		    if (ch.time) {
			response.data[chid].time = new Date(ch.time);
			response.data[chid].recent = (Date.now() - response.data[chid].time)/1000;
		    }
		});
		$scope.mychallenges = response.data
	    });
	}
	refreshChal();
        var myinterval = $interval(refreshChal, 15750);
        $scope.$on('$destroy', function () { $interval.cancel(myinterval); });
    })
