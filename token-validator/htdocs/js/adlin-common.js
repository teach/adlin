const dn_without_dnssec = ["driivve.com.", "driivee.cloud."];

function dn_has_dnssec(dn) {
  return dn_without_dnssec.reduce((s, e) => (s && dn.indexOf(e) === -1), true)
}

const tuto_legend = {
  "danger": "Test jamais réussi",
  "warning": "Test réussi il y a plus de 5 minutes",
  "info": "Test réussi il y a plus de 2 minutes",
  "success": "Test réussi il y a moins de 2 minutes",
};

const tuto_progress = [
    {
        0: { title: "single", icon: "🕵️", label: "Shadow"},
        1: { title: "Is alive?", icon: "👋", label: "Token 1"},
        2: { title: "DMZ reached", icon: "📚", label: "Token 2"},
        3: { title: "HTTPS on + time", icon: "⏲", label: "Token 3"},
        4: { title: "DNS ok", icon: "🍰", label: "Token 4"},
        5: { title: "On Internet", icon: "🌎", label: "Token 5"},
        6: { title: "Bonus caché", icon: "b", label: "Bonus 0"},
        7: { title: "Bonus ICMP", icon: "🏓", label: "Bonus 1"},
        8: { title: "Bonus disk", icon: "💽", label: "Bonus 2"},
        9: { title: "Bonus email", icon: "📧", label: "Bonus 3"},
        10: { title: "Wg tunnel", icon: "🚇", label: "Tunnel up"},
        11: { title: "SSH key", icon: "🔑", label: "Key"},
        12: { title: "SSH evade", icon: "💊", label: "SSH"},
    },
    {
        100: { title: "Firewalled", icon: "FW", label: "Firewall"},
        101: { title: "HTTP on IP", icon: "0", label: "HTTP IP"},
        102: { title: "HTTP on associated domain", icon: "1", label: "HTTP domain"},
        103: { title: "HTTPS on associated domain", icon: "2", label: "HTTPS domain"},
        104: { title: "DNS Delegation", icon: "3", label: "DNS"},
        105: { title: "HTTP on delegated domain", icon: "4", label: "HTTP on NS"},
        106: { title: "HTTPS on delegated domain", icon: "5", label: "HTTPS on NS"},
        107: { title: "HTTPS-SNI", icon: "6", label: "HTTPS-SNI"},
        //108: { title: "Matrix Federation (bonus)", icon: "7", label: "Matrix SRV"},
        //109: { title: "Matrix Client (bonus)", icon: "8", label: "Matrix CLT"},
        110: { title: "DNSSEC (bonus)", icon: "9", label: "DNSSEC"},
    },
    /*{
        200: { title: "Firewalled", icon: "FW", label: "Firewall"},
        201: { title: "HTTP on IP", icon: "0", label: "HTTP IP"},
        202: { title: "HTTP on associated domain", icon: "1", label: "HTTP domain"},
        203: { title: "HTTPS on associated domain", icon: "2", label: "HTTPS domain"},
        204: { title: "DNS Delegation", icon: "3", label: "DNS"},
        205: { title: "HTTP on delegated domain", icon: "4", label: "HTTP on NS"},
        206: { title: "HTTPS on delegated domain", icon: "5", label: "HTTPS on NS"},
        207: { title: "HTTPS-SNI", icon: "6", label: "HTTPS-SNI"},
        208: { title: "Matrix Federation (bonus)", icon: "7", label: "Matrix SRV"},
        209: { title: "Matrix Client (bonus)", icon: "8", label: "Matrix CLT"},
        210: { title: "DNSSEC (bonus)", icon: "9", label: "DNSSEC"},
    },*/
    /*{
        200: { title: "PONG resolver", icon: "0", label: "PONG srv"},
        202: { title: "HTTP on IP (bonus)", icon: "1", label: "HTTP IP"},
        204: { title: "DNS Delegation", icon: "2", label: "DNS"},
        205: { title: "HTTP on delegated domain", icon: "3", label: "HTTP on NS"},
        206: { title: "HTTPS on delegated domain", icon: "4", label: "HTTPS on NS"},
        208: { title: "Matrix Federation", icon: "5", label: "Matrix SRV"},
        209: { title: "Matrix Client", icon: "6", label: "Matrix CLT"},
        211: { title: "RH access net", icon: "7", label: "RH net"},
        212: { title: "DG access net", icon: "8", label: "DG net"},
        213: { title: "CM access net", icon: "9", label: "CM net"},
    },*/
];
