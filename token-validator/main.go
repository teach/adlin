package main

import (
	"context"
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"strings"
	"syscall"

	"git.nemunai.re/srs/adlin/libadlin"
)

var (
	baseURL string = "/"
)

type ResponseWriterPrefix struct {
	real   http.ResponseWriter
	prefix string
}

func (r ResponseWriterPrefix) Header() http.Header {
	return r.real.Header()
}

func (r ResponseWriterPrefix) WriteHeader(s int) {
	if v, exists := r.real.Header()["Location"]; exists && len(v) > 0 && !strings.HasPrefix(v[0], "https://") && !strings.HasPrefix(v[0], "http://") {
		r.real.Header().Set("Location", r.prefix+v[0])
	}
	r.real.WriteHeader(s)
}

func (r ResponseWriterPrefix) Write(z []byte) (int, error) {
	return r.real.Write(z)
}

func StripPrefix(prefix string, h http.Handler) http.Handler {
	if prefix == "" {
		return h
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if prefix != "/" && r.URL.Path == "/" {
			http.Redirect(w, r, prefix+"/", http.StatusFound)
		} else if p := strings.TrimPrefix(r.URL.Path, prefix); len(p) < len(r.URL.Path) {
			r2 := new(http.Request)
			*r2 = *r
			r2.URL = new(url.URL)
			*r2.URL = *r.URL
			r2.URL.Path = p
			h.ServeHTTP(ResponseWriterPrefix{w, prefix}, r2)
		} else {
			h.ServeHTTP(w, r)
		}
	})
}

func main() {
	var err error

	if v, exists := os.LookupEnv("ADLIN_NS_HOST"); exists {
		ControlSocket = v
	}
	if v, exists := os.LookupEnv("ADLIN_TSIG_NAME"); exists {
		tsigName = v
	}
	if v, exists := os.LookupEnv("ADLIN_TSIG_SECRET"); exists {
		tsigSecret = v
	}
	if v, exists := os.LookupEnv("ADLIN_SHARED_SECRET"); exists {
		adlin.SharedSecret = v
	}
	if v, exists := os.LookupEnv("ADLIN_COLLECTOR_SECRET"); !exists {
		log.Println("Warning: Please define ADLIN_COLLECTOR_SECRET environment variable")
	} else if t, err := base64.StdEncoding.DecodeString(v); err != nil {
		log.Fatal("Error reading ADLIN_COLLECTOR_SECRET variable:", err)
	} else {
		adlin.SetCollectorSecret(t)
	}

	var bind = flag.String("bind", ":8081", "Bind port/socket")
	var dsn = flag.String("dsn", adlin.DSNGenerator(), "DSN to connect to the MySQL server")
	flag.StringVar(&baseURL, "baseurl", baseURL, "URL prepended to each URL")
	flag.StringVar(&adlin.SharedSecret, "sharedsecret", adlin.SharedSecret, "secret used to communicate with remote validator")
	flag.StringVar(&AuthorizedKeysLocation, "authorizedkeyslocation", AuthorizedKeysLocation, "File for allowing user to SSH to the machine")
	flag.StringVar(&SshPiperLocation, "sshPiperLocation", SshPiperLocation, "Directory containing directories for sshpiperd")
	flag.StringVar(&ControlSocket, "ns-host", ControlSocket, "Host:port of the nameserver to use")
	flag.StringVar(&tsigName, "tsig-name", tsigName, "TSIG name to use to contact NS")
	flag.StringVar(&tsigSecret, "tsig-secret", tsigSecret, "TSIG secret to use to contact NS")
	var dummyauth = flag.Bool("dummyauth", false, "don't perform password check")
	flag.Parse()

	// Sanitize options
	log.Println("Checking paths...")
	if err = sanitizeStaticOptions(); err != nil {
		log.Fatal(err)
	}
	if baseURL != "/" {
		baseURL = path.Clean(baseURL)
	} else {
		baseURL = ""
	}

	if *dummyauth {
		AuthFunc = dummyAuth
	}

	initializeOIDC()

	// Initialize contents
	log.Println("Opening database...")
	if err := adlin.DBInit(*dsn); err != nil {
		log.Fatal("Cannot open the database: ", err)
	}
	defer adlin.DBClose()

	log.Println("Creating database...")
	if err := adlin.DBCreate(); err != nil {
		log.Fatal("Cannot create database: ", err)
	}

	// Prepare graceful shutdown
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	srv := &http.Server{
		Addr:    *bind,
		Handler: StripPrefix(baseURL, Router()),
	}

	// Serve content
	go func() {
		log.Fatal(srv.ListenAndServe())
	}()
	log.Println(fmt.Sprintf("Ready, listening on %s", *bind))

	// Wait shutdown signal
	<-interrupt

	log.Print("The service is shutting down...")
	srv.Shutdown(context.Background())
	log.Println("done")
}
