package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"strings"

	"github.com/julienschmidt/httprouter"

	"git.nemunai.re/srs/adlin/libadlin"
)

func init() {
	router.GET("/api/progress", apiHandler(
		func(httprouter.Params, []byte) (interface{}, error) {
			if stds, err := adlin.GetStudents(); err != nil {
				return nil, err
			} else {
				ret := map[string]map[string]*adlin.UnlockedChallenge{}
				for _, std := range stds {
					if sts, err := std.GetStates(); err == nil {
						ret[std.Login] = map[string]*adlin.UnlockedChallenge{}

						for _, s := range sts {
							ret[std.Login][fmt.Sprintf("%d", s.Challenge)] = s
						}

						if pongs, err := std.LastPongs(); err == nil && len(pongs) > 0 {
							ret[std.Login]["ping"] = &adlin.UnlockedChallenge{
								IdStudent: std.Id,
								Time:      &pongs[0].Date,
								Value:     pongs[0].State,
							}
						} else if err != nil {
							log.Println(err)
						}
					}
				}
				return ret, nil
			}
		}))
	router.GET("/api/students/", apiHandler(
		func(httprouter.Params, []byte) (interface{}, error) {
			return adlin.GetStudents()
		}))
	router.POST("/api/students/", remoteValidatorHandler(apiHandler(createStudent)))
	router.GET("/api/students/:sid/", apiHandler(studentHandler(
		func(std *adlin.Student, _ []byte) (interface{}, error) {
			return std, nil
		})))
	router.PUT("/api/students/:sid/", remoteValidatorHandler(apiHandler(studentHandler(updateStudent))))
	router.DELETE("/api/students/:sid/", remoteValidatorHandler(apiHandler(studentHandler(
		func(std *adlin.Student, _ []byte) (interface{}, error) {
			return std.Delete()
		}))))
	router.GET("/api/students/:sid/progress", apiHandler(studentHandler(
		func(std *adlin.Student, _ []byte) (interface{}, error) {
			ret := map[string]*adlin.UnlockedChallenge{}

			if sts, err := std.GetStates(); err == nil {
				for _, s := range sts {
					ret[fmt.Sprintf("%d", s.Challenge)] = s
				}
			}

			if cerrors, err := std.GetChallengeErrors(); err == nil {
				for _, cerr := range cerrors {
					if _, ok := ret[fmt.Sprintf("%d", cerr.Challenge)]; ok {
						ret[fmt.Sprintf("%d", cerr.Challenge)].Error = cerr.Error
						ret[fmt.Sprintf("%d", cerr.Challenge)].LastCheck = &cerr.Time
					} else {
						ret[fmt.Sprintf("%d", cerr.Challenge)] = &adlin.UnlockedChallenge{
							LastCheck: &cerr.Time,
							Error:     cerr.Error,
						}
					}
				}
			}

			return ret, nil
		})))
}

type uploadedStudent struct {
	Login string `json:"login"`
	IP    string `json:"ip"`
	MAC   string `json:"mac"`
}

func createStudent(_ httprouter.Params, body []byte) (interface{}, error) {
	var err error
	var std uploadedStudent
	if err = json.Unmarshal(body, &std); err != nil {
		return nil, err
	}

	var exist *adlin.Student
	if exist, err = adlin.GetStudentByLogin(strings.TrimSpace(std.Login)); err != nil {
		if exist, err = adlin.NewStudent(strings.TrimSpace(std.Login)); err != nil {
			return nil, err
		}
	}
	exist.RegisterAccess(std.IP, std.MAC)

	ip := IPSuffix(exist, net.IPNet{IP: net.ParseIP("172.23.0.0"), Mask: net.CIDRMask(17, 32)}).String()
	exist.IP = &ip

	return exist, nil
}

func updateStudent(current *adlin.Student, body []byte) (interface{}, error) {
	new := &adlin.Student{}
	if err := json.Unmarshal(body, &new); err != nil {
		return nil, err
	}

	current.Login = new.Login
	current.Time = new.Time
	return current.Update()
}
