package main

import (
	"github.com/julienschmidt/httprouter"

	"git.nemunai.re/srs/adlin/libadlin"
)

func init() {
	router.GET("/api/grades/", remoteValidatorHandler(apiHandler(computeGrades)))
}

func computeGrades(_ httprouter.Params, _ []byte) (interface{}, error) {
	if stds, err := adlin.GetStudents(); err != nil {
		return nil, err
	} else {
		res := map[string]map[string]float32{}

		for _, std := range stds {
			res[std.Login] = map[string]float32{
				"TP1": 0,
				"TP2": 0,
				"TP3": 0,
			}

			if states, err := std.GetStatesByChallenge(); err != nil {
				return nil, err
			} else {
				for _, st := range states {
					if st.Challenge >= 200 {
						switch st.Challenge {
						case 200:
							res[std.Login]["TP3"] += 3
						case 201:
							res[std.Login]["TP3"] += 2
						case 203:
							res[std.Login]["TP3"] += 2
						case 204:
							res[std.Login]["TP3"] += 2
						case 205:
							res[std.Login]["TP3"] += 2
						case 206:
							res[std.Login]["TP3"] += 1
						case 207:
							res[std.Login]["TP3"] += 2
						case 208:
							res[std.Login]["TP3"] += 2
						case 209:
							res[std.Login]["TP3"] += 2
						case 210:
							res[std.Login]["TP3"] += 2
						}
					} else if st.Challenge >= 100 {
						switch st.Challenge {
						case 100:
							res[std.Login]["TP2"] += 2
						case 101:
							res[std.Login]["TP2"] += 2
						case 102:
							res[std.Login]["TP2"] += 1
						case 103:
							res[std.Login]["TP2"] += 3
						case 104:
							res[std.Login]["TP2"] += 3
						case 105:
							res[std.Login]["TP2"] += 2
						case 106:
							res[std.Login]["TP2"] += 2
						case 107:
							res[std.Login]["TP2"] += 3
						case 110:
							res[std.Login]["TP2"] += 2
						}
					} else {
						switch st.Challenge {
						case 0:
							res[std.Login]["TP1"] += 1
						case 1:
							res[std.Login]["TP1"] += 3
						case 2:
							res[std.Login]["TP1"] += 3
						case 3:
							res[std.Login]["TP1"] += 3
						case 4:
							res[std.Login]["TP1"] += 2
						case 5:
							res[std.Login]["TP1"] += 2
						case 6:
							res[std.Login]["TP1"] += 1
						case 7:
							res[std.Login]["TP1"] += 1
						case 8:
							res[std.Login]["TP1"] += 1
						case 9:
							res[std.Login]["TP1"] += 1
						case 10:
							res[std.Login]["TP1"] += 2
						case 11:
							res[std.Login]["TP1"] += 1
						case 12:
							res[std.Login]["TP1"] += 1
						}
					}
				}
			}
		}

		return res, nil
	}
}
