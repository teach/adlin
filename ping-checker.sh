#!/bin/sh

[ -n "$SECRET_KEY" ] || { echo "Please define SECRET_KEY"; exit 1; }

CNT=$(curl -s https://adlin.nemunai.re/api/ips)


# First, generate static ARP table

echo "${CNT}" | jq -r '. | keys | .[]' - | while read sid
do
    MAC=$(echo "${CNT}" | jq -r '.["'$sid'"].mac' -)
    if [ -n "$MAC" ]
    then
	echo "${CNT}" | jq -r '.["'$sid'"].vlan0,.["'$sid'"].vlan7' - | while read IP
	do
	    arp -s "${IP}" "${MAC}"
	done
    fi
done


# Then, perform checks

echo "${CNT}" | jq -r '. | keys | .[]' - | while read sid
do
    IP=$(echo "${CNT}" | jq -r '.["'$sid'"].vlan0' -)
    if [ -n "${IP}" ]
    then
	case $(($sid % 5)) in
	    1)
		PAT=aabaadaa;;
	    2)
		PAT=adba0df0;;
	    3)
		PAT=ddbafeca;;
	    4)
		PAT=fecabeba;;
	    *)
		PAT=addeefbe;;
	esac

	arping -I br-ext -c 1 -w 1 "${IP}" && {
	    ping -c 1 -w 1 -p "${PAT}" "${IP}"
	    STATE=$?
	    curl -k -X POST -d '{"challenge": '${STATE}', "token": "'${SECRET_KEY}'"}' "https://172.23.200.1/api/students/$sid/pong"
	}
    fi
done
