package adlin

import (
	"crypto/rand"
	"time"
)

type Session struct {
	Id        []byte    `json:"id"`
	IdStudent *int64    `json:"login"`
	Time      time.Time `json:"time"`
}

func GetSession(id []byte) (s *Session, err error) {
	s = new(Session)
	err = DBQueryRow("SELECT id_session, id_student, time FROM student_sessions WHERE id_session=?", id).Scan(&s.Id, &s.IdStudent, &s.Time)
	return
}

func NewSession() (*Session, error) {
	session_id := make([]byte, 255)
	if _, err := rand.Read(session_id); err != nil {
		return nil, err
	} else if _, err := DBExec("INSERT INTO student_sessions (id_session, time) VALUES (?, ?)", session_id, time.Now()); err != nil {
		return nil, err
	} else {
		return &Session{session_id, nil, time.Now()}, nil
	}
}

func (student *Student) NewSession() (*Session, error) {
	session_id := make([]byte, 255)
	if _, err := rand.Read(session_id); err != nil {
		return nil, err
	} else if _, err := DBExec("INSERT INTO student_sessions (id_session, id_student, time) VALUES (?, ?, ?)", session_id, student.Id, time.Now()); err != nil {
		return nil, err
	} else {
		return &Session{session_id, &student.Id, time.Now()}, nil
	}
}

func (s *Session) SetStudent(student *Student) (*Session, error) {
	s.IdStudent = &student.Id
	_, err := s.Update()
	return s, err
}

func (s *Session) Update() (int64, error) {
	if res, err := DBExec("UPDATE student_sessions SET id_student = ?, time = ? WHERE id_session = ?", s.IdStudent, s.Time, s.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (s *Session) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM student_sessions WHERE id_session = ?", s.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearSession() (int64, error) {
	if res, err := DBExec("DELETE FROM student_sessions"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
