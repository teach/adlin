package adlin

import (
	"fmt"
	"strings"
)

var (
	AssociatedDomainSuffixes = []string{"adlin2024.driive.online.", "adlin2024.driive.site.", "adlin2024.drivve.online.", "adlin2024.drivee.site.", "adlin2024.p0m.fr."}
	DelegatedDomainSuffixes  = []string{"srs.driive.online.", "srs.driive.site.", "srs.drivve.online.", "srs.drivee.site.", "srs.p0m.fr."}
)

func (student *Student) MyDelegatedDomainSuffix() string {
	if student.DelegatedDomain != nil {
		for _, ddomain := range DelegatedDomainSuffixes {
			if strings.HasSuffix(*student.DelegatedDomain, ddomain) {
				return ddomain
			}
		}
	}

	return DelegatedDomainSuffixes[int(student.Id)%len(DelegatedDomainSuffixes)]
}

func (student *Student) MyDelegatedDomain() string {
	if student.DelegatedDomain != nil {
		return *student.DelegatedDomain
	}

	return fmt.Sprintf("%s.%s", strings.Trim(strings.Replace(student.Login, "_", "-", -1), "-_"), student.MyDelegatedDomainSuffix())
}

func (student *Student) MyAssociatedDomainSuffix() string {
	return AssociatedDomainSuffixes[int(student.Id)%len(AssociatedDomainSuffixes)]
}

func (student *Student) DefaultAssociatedDomain() string {
	return fmt.Sprintf("%s.%s", strings.Trim(strings.Replace(student.Login, "_", "-", -1), "-_"), student.MyAssociatedDomainSuffix())
}

func (student *Student) MyAssociatedDomain() string {
	if student.AssociatedDomain != nil {
		return *student.AssociatedDomain
	}

	return student.DefaultAssociatedDomain()
}

func (student *Student) GetAssociatedDomains() (ds []string) {
	defdn := student.DefaultAssociatedDomain()
	ds = append(ds, defdn)

	studentDomain := student.MyAssociatedDomain()

	if defdn != studentDomain {
		ds = append(ds, studentDomain)
	}

	return
}
