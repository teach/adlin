package adlin

import (
	"time"
)

type Pong struct {
	Date  time.Time
	Count uint
	State bool
}

func (s *Student) LastPongs() (pongs []*Pong, err error) {
	if rows, errr := DBQuery("SELECT last, count, state FROM student_pong WHERE id_student = ? ORDER BY last DESC", s.Id); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		for rows.Next() {
			p := &Pong{}
			if err = rows.Scan(&p.Date, &p.Count, &p.State); err != nil {
				return
			}
			pongs = append(pongs, p)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func (s *Student) OnPong(state bool) (err error) {
	_, err = DBExec("INSERT INTO student_pong (id_student, last, state) VALUES (?, ?, ?)", s.Id, time.Now(), state)
	if err != nil {
		_, err = DBExec("UPDATE student_pong SET last = CURRENT_TIMESTAMP, count = count + 1 WHERE id_student = ? AND state = ?", s.Id, state)
	}
	return
}
