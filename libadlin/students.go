package adlin

import (
	"crypto/hmac"
	"crypto/sha512"
	"fmt"
	"time"
)

type Student struct {
	Id               int64      `json:"id"`
	Login            string     `json:"login"`
	Time             *time.Time `json:"time"`
	IP               *string    `json:"ip"`
	MAC              *string    `json:"mac"`
	AssociatedDomain *string    `json:"associated_domain,omitempty"`
	DelegatedDomain  *string    `json:"delegated_domain,omitempty"`
}

func GetStudents() (students []*Student, err error) {
	if rows, errr := DBQuery("SELECT S.id_student, S.login, MAX(L.time), L.ip, L.mac, S.associatedDomain, S.delegatedDomain FROM students S INNER JOIN (SELECT a.id_student, a.time, a.ip, a.mac FROM student_login a INNER JOIN (SELECT id_student, MAX(time) AS time FROM student_login GROUP BY id_student) b ON a.id_student = b.id_student AND a.time = b.time) L ON S.id_student = L.id_student GROUP BY id_student"); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		for rows.Next() {
			s := &Student{}
			if err = rows.Scan(&s.Id, &s.Login, &s.Time, &s.IP, &s.MAC, &s.AssociatedDomain, &s.DelegatedDomain); err != nil {
				return
			}
			students = append(students, s)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func GetStudent(id int) (s *Student, err error) {
	s = new(Student)
	err = DBQueryRow("SELECT S.id_student, S.login, MAX(L.time), L.ip, L.mac, S.associatedDomain, S.delegatedDomain FROM students S INNER JOIN (SELECT a.id_student, a.time, a.ip, a.mac FROM student_login a INNER JOIN (SELECT id_student, MAX(time) AS time FROM student_login GROUP BY id_student) b ON a.id_student = b.id_student AND a.time = b.time) L ON S.id_student = L.id_student WHERE S.id_student=?", id).Scan(&s.Id, &s.Login, &s.Time, &s.IP, &s.MAC, &s.AssociatedDomain, &s.DelegatedDomain)
	return
}

func GetStudentByLogin(login string) (s *Student, err error) {
	s = new(Student)
	err = DBQueryRow("SELECT S.id_student, S.login, MAX(L.time), L.ip, L.mac, S.associatedDomain, S.delegatedDomain FROM students S INNER JOIN (SELECT a.id_student, a.time, a.ip, a.mac FROM student_login a INNER JOIN (SELECT id_student, MAX(time) AS time FROM student_login GROUP BY id_student) b ON a.id_student = b.id_student AND a.time = b.time) L ON S.id_student = L.id_student WHERE login=?", login).Scan(&s.Id, &s.Login, &s.Time, &s.IP, &s.MAC, &s.AssociatedDomain, &s.DelegatedDomain)
	return
}

func StudentExists(login string) bool {
	var z int
	err := DBQueryRow("SELECT 1 FROM students WHERE login=?", login).Scan(&z)
	return err == nil && z == 1
}

func NewStudent(login string) (*Student, error) {
	t := time.Now()
	if res, err := DBExec("INSERT INTO students (login, time) VALUES (?, ?)", login, t); err != nil {
		return nil, err
	} else if sid, err := res.LastInsertId(); err != nil {
		return nil, err
	} else {
		return &Student{sid, login, &t, nil, nil, nil, nil}, nil
	}
}

func (s *Student) GetPKey() []byte {
	h := hmac.New(sha512.New512_224, []byte(SharedSecret))
	h.Write([]byte(s.Login))
	return h.Sum(nil)
}

func (s *Student) Update() (int64, error) {
	if res, err := DBExec("UPDATE students SET login = ?, time = ?, associatedDomain = ?, delegatedDomain = ? WHERE id_student = ?", s.Login, s.Time, s.AssociatedDomain, s.DelegatedDomain, s.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (s *Student) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM students WHERE id_student = ?", s.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearStudents() (int64, error) {
	if res, err := DBExec("DELETE FROM students"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

type UnlockedChallenge struct {
	Id        int64       `json:"id,omitempty"`
	IdStudent int64       `json:"id_student"`
	Challenge int         `json:"challenge,omitempty"`
	Time      *time.Time  `json:"time,omitempty"`
	Value     interface{} `json:"value,omitempty"`
	LastCheck *time.Time  `json:"last_check,omitempty"`
	Error     string      `json:"error,omitempty"`
}

func (s *Student) GetStates() (ucs []*UnlockedChallenge, err error) {
	if rows, errr := DBQuery("SELECT id_st, challenge, time FROM student_challenges WHERE id_student = ?", s.Id); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		for rows.Next() {
			u := &UnlockedChallenge{}
			u.IdStudent = s.Id
			if err = rows.Scan(&u.Id, &u.Challenge, &u.Time); err != nil {
				return
			}
			ucs = append(ucs, u)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func (s *Student) GetChallengeError(challenge_id int) (uc *ErroredChallenge, err error) {
	uc = new(ErroredChallenge)
	err = DBQueryRow("SELECT id_st, challenge, time, error FROM student_challenge_errors WHERE id_student = ? AND challenge = ?", s.Id, challenge_id).Scan(&uc.Id, &uc.Challenge, &uc.Time, &uc.Error)
	return
}

func (s *Student) GetChallengeErrors() (ucs []*ErroredChallenge, err error) {
	if rows, errr := DBQuery("SELECT id_st, challenge, time, error FROM student_challenge_errors WHERE id_student = ?", s.Id); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		for rows.Next() {
			u := &ErroredChallenge{}
			u.IdStudent = s.Id
			if err = rows.Scan(&u.Id, &u.Challenge, &u.Time, &u.Error); err != nil {
				return
			}
			ucs = append(ucs, u)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func (s *Student) GetStatesByChallenge() (ucs []*UnlockedChallenge, err error) {
	if rows, errr := DBQuery("SELECT id_st, challenge, MIN(time), value FROM student_challenges WHERE id_student = ? GROUP BY challenge, id_student", s.Id); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		for rows.Next() {
			u := &UnlockedChallenge{}
			u.IdStudent = s.Id
			if err = rows.Scan(&u.Id, &u.Challenge, &u.Time, &u.Value); err != nil {
				return
			}
			ucs = append(ucs, u)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func (s *Student) UnlockChallenge(challenge int, value string) (uc *UnlockedChallenge, err error) {
	if uc, err = s.UnlockNewChallenge(challenge, value); err != nil {
		if uc, err = s.UpdateUnlockedChallenge(challenge, value); err != nil {
			return
		}
	}

	s.RegisterChallengeError(challenge, fmt.Errorf("OK"))

	return
}

func (s *Student) UnlockNewChallenge(challenge int, value string) (*UnlockedChallenge, error) {
	if res, err := DBExec("INSERT INTO student_challenges (id_student, challenge, time, value) VALUES (?, ?, ?, ?)", s.Id, challenge, time.Now(), value); err != nil {
		return nil, err
	} else if utid, err := res.LastInsertId(); err != nil {
		return nil, err
	} else {
		now := time.Now()
		return &UnlockedChallenge{utid, s.Id, challenge, &now, value, nil, ""}, err
	}
}

func (s *Student) UpdateUnlockedChallenge(challenge int, value string) (*UnlockedChallenge, error) {
	if _, err := DBExec("UPDATE student_challenges SET time = ?, value = ? WHERE id_student = ? AND challenge = ?", time.Now(), value, s.Id, challenge); err != nil {
		return nil, err
	} else {
		now := time.Now()
		return &UnlockedChallenge{0, s.Id, challenge, &now, value, nil, ""}, err
	}
}

type ErroredChallenge struct {
	Id        int64     `json:"id,omitempty"`
	IdStudent int64     `json:"id_student"`
	Challenge int       `json:"challenge,omitempty"`
	Time      time.Time `json:"time"`
	Error     string    `json:"error,omitempty"`
}

func (s *Student) RegisterChallengeError(challenge int, err error) error {
	if _, errr := DBExec("INSERT INTO student_challenge_errors (id_student, challenge, time, error) VALUES (?, ?, ?, ?)", s.Id, challenge, time.Now(), err.Error()); errr == nil {
		return nil
	} else if _, errr := DBExec("UPDATE student_challenge_errors SET time = ?, error = ? WHERE id_student = ? AND challenge = ?", time.Now(), err.Error(), s.Id, challenge); errr != nil {
		return errr
	} else {
		return nil
	}
}

func (s *Student) RegisterAccess(ip, mac string) error {
	if res, err := DBExec("INSERT INTO student_login (id_student, ip, mac, time) VALUES (?, ?, ?, ?)", s.Id, ip, mac, time.Now()); err != nil {
		return err
	} else if _, err := res.LastInsertId(); err != nil {
		return err
	} else {
		return err
	}
}
