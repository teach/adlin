package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"git.nemunai.re/srs/adlin/libadlin"
)

func main() {
	offline := false

	var dsn = flag.String("dsn", adlin.DSNGenerator(), "DSN to connect to the MySQL server")
	flag.BoolVar(&verbose, "verbose", verbose, "Enable verbose mode")
	flag.BoolVar(&verbose2, "verbose2", verbose2, "Enable more verbose mode")
	flag.BoolVar(&offline, "offline", offline, "Enable offline mode (doesn't check what wg report)")
	flag.Parse()

	if verbose2 && !verbose {
		verbose = verbose2
	}

	// Initialize contents
	log.Println("Opening database...")
	if err := adlin.DBInit(*dsn); err != nil {
		log.Fatal("Cannot open the database: ", err)
	}
	defer adlin.DBClose()

	log.Println("Creating database...")
	if err := adlin.DBCreate(); err != nil {
		log.Fatal("Cannot create database: ", err)
	}

	if len(flag.Args()) >= 1 {
		// Check one or more students and exit
		for _, std_login := range flag.Args() {
			if verbose {
				log.Printf("==============================\n\n")
			}

			if std, err := adlin.GetStudentByLogin(std_login); err != nil {
				log.Printf("%s: %s", std_login, err.Error())
			} else {
				log.Printf("Checking %s...", std.Login)
				studentChecker(std, true, offline)
			}
		}

		return
	}

	// Prepare graceful shutdown
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	ticker := time.NewTicker(30 * time.Second)
	defer ticker.Stop()

	// Launch checker
	studentsChecker(offline)
loop:
	for {
		select {
		case <-interrupt:
			break loop
		case <-ticker.C:
			studentsChecker(offline)
		}
	}
}
