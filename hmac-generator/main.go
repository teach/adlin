package main

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"os"
	"time"
)

func main() {
	sharedSecret := "adelina"
	if len(os.Args) > 1 {
		sharedSecret = os.Args[1]
	}

	h := hmac.New(sha512.New, []byte(sharedSecret))
	h.Write([]byte(fmt.Sprintf("%d", time.Now().Unix()/10)))

	fmt.Println(base64.StdEncoding.EncodeToString(h.Sum(nil)))
}
