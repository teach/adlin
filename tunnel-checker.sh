#!/bin/sh

[ -n "$SECRET_KEY" ] || { echo "Please define SECRET_KEY"; exit 1; }

while true
do

CNT=$(curl -s https://adlin.nemunai.re/api/ips)


# Perform checks

echo "${CNT}" | jq -r '. | keys | .[]' - | while read sid
do
    IP=$(echo "${CNT}" | jq -r '.["'$sid'"].wg' -)
    [ -n "${IP}" ] && (
	echo -ne "\n${sid}: "
	ping6 -q -c 1 -w 1 "${IP}1" > /dev/null && {
	    echo -n "PONG "
	    curl -k -X POST -d '{"token": "'${SECRET_KEY}'"}' -H "X-Forwarded-Proto: https" "https://adlin.nemunai.re/api/students/$sid/pong"

	    # TUTO2 checks

	    # port 80
	    PAGE=$(curl -f -s "http://[${IP}1]/") &&
		curl -X POST -d @- "https://adlin.nemunai.re/challenge/100" <<EOF
{
  "token": "${SECRET_KEY}",
  "login": "${sid}",
  "data": ["$(echo ${PAGE} | sha1sum | cut -d ' ' -f 1)"]
}
EOF

	    # port 443
	    PAGE=$(echo | busybox timeout -t 3 openssl s_client -host "${IP}1" -port 443 2> /dev/null) &&
		curl -X POST -d @- "https://adlin.nemunai.re/challenge/101" <<EOF
{
  "token": "${SECRET_KEY}",
  "login": "${sid}",
  "data": ["$(echo ${PAGE} | sha1sum | cut -d ' ' -f 1)"]
}
EOF

	    DDN=$(echo "${CNT}" | jq -r '.["'$sid'"].ddn' -)
	    if [ -n "${DDN}" ]
	    then
		# port 53
		DIG=$(LD_PRELOAD=/usr/lib/gcc/armv5tel-softfloat-linux-gnueabi/8.2.0/libgcc_s.so.1 kdig +time=1 +noretry +short "@${IP}1" "${DDN}" SOA 2> /dev/null)
		[ -n "${DIG}" ] &&
		    curl -X POST -d @- "https://adlin.nemunai.re/challenge/102" <<EOF
{
  "token": "${SECRET_KEY}",
  "login": "${sid}",
  "data": ["${DIG}"]
}
EOF
	    fi
	}
    )
done


done
